#!/bin/bash
set -euo pipefail

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd $SCRIPTPATH/..

RASPBERRY=foodcoop@raspberrypi
WORK_DIR=/home/foodcoop/foodcoop/

rsync -avz -e 'ssh' --exclude=dist.sqlite --delete dist/ raspberry/Dockerfile raspberry/docker-compose.yaml $RASPBERRY:$WORK_DIR

#build application image
ssh -t $RASPBERRY 'cd '$WORK_DIR'; docker build -t foodcoop:latest -f Dockerfile . --pull'
 
ssh -t $RASPBERRY 'cd '$WORK_DIR'; docker-compose down || true'
ssh -t $RASPBERRY 'cd '$WORK_DIR'; docker-compose run -d -w /usr/src/app foodcoop' 

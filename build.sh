#!/bin/bash
set -euo pipefail

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd $SCRIPTPATH

docker run --name foodcoop --rm -v"$SCRIPTPATH":/source -w /source node:lts-buster /usr/local/bin/npm run build

#FIX: modification date not updated
touch $SCRIPTPATH/dist/client/index.html

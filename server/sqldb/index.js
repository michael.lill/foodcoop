/**
 * Sequelize initialization module
 */

'use strict';

var config = require('../config/environment');
const { Sequelize, DataTypes } = require("sequelize");

console.log(`sequelize.uri: ${config.sequelize.uri}`)
var db = {
  Sequelize,
  sequelize: new Sequelize(config.sequelize.uri, config.sequelize.options)
};

// Insert models below
db.Orderable = require(`../api/orderable/orderable.model`)(db.sequelize, Sequelize.DataTypes);;
db.Product = require(`../api/product/product.model`)(db.sequelize, Sequelize.DataTypes);;
db.User = require(`../api/user/user.model`)(db.sequelize, Sequelize.DataTypes);;
db.Setting = require(`../api/setting/setting.model`)(db.sequelize, Sequelize.DataTypes);;
db.Message = require(`../api/message/message.model`)(db.sequelize, Sequelize.DataTypes);;
db.Nonperishable = require(`../api/nonperishable/nonperishable.model`)(db.sequelize, Sequelize.DataTypes);;

db.ProductTotal = db.sequelize.define('product_total', {
  _id: {
    primaryKey: true,
    type: DataTypes.INTEGER
  },
  total: DataTypes.FLOAT
}, { freezeTableName: true, timestamps: false });

db.Product.hasOne(db.ProductTotal, {
  foreignKey: '_id'
});

db.Orderable.hasMany(db.Product, { as: 'Products' });
db.User.hasMany(db.Product, { as: 'Products' });
db.Product.belongsTo(db.Orderable);
db.Product.belongsTo(db.User);
db.Message.belongsTo(db.User);

var schedule = require('node-schedule');
schedule.scheduleJob('00 00 02 * * *', async function () {
  await db.sequelize.query(`delete from "Messages" where "createdAt" <= (now() - interval '30 days')`);
  console.log('Old messages deleted.');
});

module.exports = db;

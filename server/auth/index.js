'use strict';

var express = require('express');
var config = require('../config/environment');
var User = require('../sqldb').User;

// Passport Configuration
require('./local/passport').setup(User, config);

var router = express.Router();

router.use('/local', require('./local').default);

module.exports = router;

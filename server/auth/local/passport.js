import passport from 'passport';
import {Strategy as LocalStrategy} from 'passport-local';

function localAuthenticate(User, name, password, done) {
  User.find({
    where: {
      name
    }
  })
    .then(user => {
      if(!user) {
        return done(null, false, {
          message: 'Unbekannter Haushalt.'
        });
      }
      user.authenticate(password, function(authError, authenticated) {
        if(authError) {
          return done(authError);
        }
        if(!authenticated) {
          return done(null, false, { message: 'Passwort falsch.' });
        } else {
          return done(null, user);
        }
      });
      return user;
    })
    .catch(err => done(err));
}

export function setup(User) {
  passport.use(new LocalStrategy({
    usernameField: 'name',
    passwordField: 'password' // this is the virtual field on the model
  }, function(name, password, done) {
    return localAuthenticate(User, name, password, done);
  }));
}

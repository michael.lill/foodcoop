var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

function localAuthenticate(User, name, password, done) {
  User.findOne({
    where: {
      name
    }
  })
    .then(user => {
      if(!user) {
        return done(null, false, {
          message: 'Unbekannter Haushalt.'
        });
      }
      user.authenticate(password, function(authError, authenticated) {
        if(authError) {
          return done(authError);
        }
        if(!authenticated) {
          return done(null, false, { message: 'Passwort falsch.' });
        } else {
          return done(null, user);
        }
      });
      return user;
    })
    .catch(err => done(err));
}

exports.setup = function(User) {
  passport.use(new LocalStrategy({
    usernameField: 'name',
    passwordField: 'password' // this is the virtual field on the model
  }, function(name, password, done) {
    return localAuthenticate(User, name, password, done);
  }));
}

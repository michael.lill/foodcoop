function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

function validationError(res, statusCode) {
  statusCode = statusCode || 422;
  return function(err) {
    res.status(statusCode).json(err);
  };
}

function saveUpdates(updates) {
  return function(entity) {
    return entity.update(updates)
      .then(updated => updated);
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function spreadQueryResult(queryResults) {
  for (let index = 0; index < queryResults.length; index++) {
    const queryResult = queryResults[index];
    for (const key in queryResult) {
      if (queryResult.hasOwnProperty(key)) {
        const element = queryResult[key];
        if (key.indexOf('.') > 0) {
          if(![null, undefined].includes(element)){
            queryResult[key.split('.')[0]] = queryResult[key.split('.')[0]] || {};
            queryResult[key.split('.')[0]][key.split('.')[1]] = element;
          }else{
            queryResult[key.split('.')[0]] = queryResult[key.split('.')[0]] || null;
          }
          delete queryResult[key];
        }
      }
    }
  }
  return queryResults;
}

function morphProducts(products) {
  products.forEach(morphProduct);
  return products;
}

function morphProduct(product) {
  if(!!product.product_total){
    product.price = product.product_total.total;
    delete product.product_total;
  }
  return product;
}

function select(sqldb) {
  return (query) => sqldb.sequelize.query(query, { type: sqldb.sequelize.QueryTypes.SELECT });
}

module.exports = {
  respondWithResult,
  handleError,
  validationError,
  saveUpdates,
  handleEntityNotFound,
  removeEntity,
  spreadQueryResult,
  morphProducts,
  morphProduct,
  select
};

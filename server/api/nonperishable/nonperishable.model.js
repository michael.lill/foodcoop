'use strict';

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Nonperishable', {
    _id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    supplier: DataTypes.STRING,
    name: DataTypes.STRING,
    pricePerQuantity: DataTypes.FLOAT,
    unit: DataTypes.STRING
  });
}

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/nonperishables              ->  index
 * POST    /api/nonperishables              ->  create
 * GET     /api/nonperishables/:id          ->  show
 * PUT     /api/nonperishables/:id          ->  update
 * DELETE  /api/nonperishables/:id          ->  destroy
 */

'use strict';

var sqldb = require('../../sqldb');
var helpers = require('../helpers');

// Gets a list of Nonperishables
exports.index = function(req, res) {
  return sqldb.Nonperishable.findAll({
    order: [['createdAt', 'ASC']]
  })
    .then(helpers.respondWithResult(res))
    .catch(helpers.handleError(res));
}

// Gets a single Nonperishable from the DB
exports.show = function(req, res) {
  return sqldb.Nonperishable.findOne({
    where: {
      _id: req.params.id
    }
  })
    .then(helpers.handleEntityNotFound(res))
    .then(helpers.respondWithResult(res))
    .catch(helpers.handleError(res));
}

// Creates a new Nonperishable in the DB
exports.create = async function(req, res) {
  try{
    const entity = await sqldb.Nonperishable.create(req.body);
    return helpers.respondWithResult(res, 201)(entity);
  }catch(error){
    return helpers.handleError(res)(error);
  }
}

// Updates an existing Nonperishable in the DB
exports.update = function(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return sqldb.Nonperishable.findOne({
    where: {
      _id: req.params.id
    }
  })
    .then(helpers.handleEntityNotFound(res))
    .then(helpers.saveUpdates(req.body))
    .then(helpers.respondWithResult(res))
    .catch(helpers.handleError(res));
}

// Deletes a Nonperishable from the DB
exports.destroy = function(req, res) {
  return sqldb.Nonperishable.findOne({
    where: {
      _id: req.params.id
    }
  })
    .then(helpers.handleEntityNotFound(res))
    .then(helpers.removeEntity(res))
    .catch(helpers.handleError(res));
}

'use strict';

var crypto = require('crypto');

var validatePresenceOf = function (value) {
  return value && value.length;
};

module.exports = function (sequelize, DataTypes) {
  var User = sequelize.define('User', {

    _id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      unique: {
        msg: 'The specified name is already in use.'
      },
    },
    email: {
      type: DataTypes.STRING,
      unique: {
        msg: 'The specified email address is already in use.'
      }
    },
    role: {
      type: DataTypes.STRING,
      defaultValue: 'user'
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    provider: DataTypes.STRING,
    salt: DataTypes.STRING

  }, {

    /**
     * Virtual Getters
     */
    getterMethods: {
      // Public profile information
      profile: function () {
        return {
          name: this.name,
          role: this.role
        };
      },

      // Non-sensitive info we'll be putting in the token
      token: function () {
        return {
          _id: this._id,
          role: this.role
        };
      }
    },

    /**
     * Pre-save hooks
     */
    hooks: {
      beforeBulkCreate(users, fields) {
        return new Promise((resolve, reject) => {
          var totalUpdated = 0;
          users.forEach(user => {
            user.updatePassword(err => {
              if (err) {
                return reject(err);
              }
              totalUpdated += 1;
              if (totalUpdated === users.length) {
                return resolve();
              }
            });
          });
        });
      },
      beforeCreate(user, fields) {
        return user.updatePassword();
      },
      beforeUpdate(user, fields) {
        if (user.changed('password')) {
          return user.updatePassword();
        }
        return Promise.resolve();
      }
    }
  });

  /**
   * Authenticate - check if the passwords are the same
   *
   * @param {String} password
   * @param {Function} callback
   * @return {Boolean}
   * @api public
   */
  User.prototype.authenticate = function (password, callback) {
    if (!callback) {
      return this.password === this.encryptPassword(password);
    }

    var _this = this;
    this.encryptPassword(password, function (err, pwdGen) {
      if (err) {
        return callback(err);
      }

      if (_this.password === pwdGen) {
        return callback(null, true);
      } else {
        return callback(null, false);
      }
    });
  };

  /**
   * Make salt
   *
   * @param {Number} [byteSize] - Optional salt byte size, default to 16
   * @param {Function} callback
   * @return {String}
   * @api public
   */
  User.prototype.makeSalt = function (...args) {
    let byteSize;
    let callback;
    let defaultByteSize = 16;

    if (typeof arguments[0] === 'function') {
      callback = arguments[0];
      byteSize = defaultByteSize;
    } else if (typeof arguments[1] === 'function') {
      callback = arguments[1];
    } else {
      throw new Error('Missing Callback');
    }

    if (!byteSize) {
      byteSize = defaultByteSize;
    }

    return crypto.randomBytes(byteSize, function (err, salt) {
      if (err) {
        return callback(err);
      }
      return callback(null, salt.toString('base64'));
    });
  };

  /**
   * Encrypt password
   *
   * @param {String} password
   * @param {Function} callback
   * @return {String}
   * @api public
   */
  User.prototype.encryptPassword = function (password, callback) {
    if (!password || !this.salt) {
      return callback ? callback(null) : null;
    }

    var defaultIterations = 10000;
    var defaultKeyLength = 64;
    var salt = new Buffer(this.salt, 'base64');

    if (!callback) {
      // eslint-disable-next-line no-sync
      return crypto.pbkdf2Sync(password, salt, defaultIterations, defaultKeyLength, 'sha1')
        .toString('base64');
    }

    return crypto.pbkdf2(password, salt, defaultIterations, defaultKeyLength, 'sha1',
      function (err, key) {
        if (err) {
          return callback(err);
        }
        return callback(null, key.toString('base64'));
      });
  };

  /**
   * Update password field
   * @return {String}
   * @api public
   */
  User.prototype.updatePassword = function () {
    return new Promise((resolve, reject) => {
      if (!validatePresenceOf(this.password)) {
        reject(new Error('Invalid password'));
      }

      // Make salt with a callback
      this.makeSalt((saltErr, salt) => {
        if (saltErr) {
          return reject(saltErr);
        }
        this.salt = salt;
        this.encryptPassword(this.password, (encryptErr, hashedPassword) => {
          if (encryptErr) {
            reject(encryptErr);
          }
          this.password = hashedPassword;
          resolve();
        });
      });
    });
  };

  return User;
};

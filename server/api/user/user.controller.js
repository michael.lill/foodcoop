'use strict';

var config = require('../../config/environment');
var jwt = require('jsonwebtoken');

var sqldb = require('../../sqldb');
var helpers = require('../helpers');

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function (req, res) {
  return sqldb.User.findAll({
    attributes: [
      '_id',
      'name'
    ]
  })
    .then(users => {
      res.status(200).json(users);
    })
    .catch(helpers.handleError(res));
}

/**
 * Creates a new user
 */
exports.create = function (req, res) {
  var newUser = sqldb.User.build(req.body);
  newUser.setDataValue('provider', 'local');
  newUser.setDataValue('role', 'user');
  return newUser.save()
    .then(function (user) {
      var token = jwt.sign({ _id: user._id }, config.secrets.session, {
        expiresIn: 60 * 60 * 5
      });
      res.json({ token });
    })
    .catch(helpers.validationError(res));
}

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id;

  return sqldb.User.findOne({
    where: {
      _id: userId
    }
  })
    .then(user => {
      if (!user) {
        return res.status(404).end();
      }
      res.json(user.profile);
    })
    .catch(err => next(err));
}

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function (req, res) {
  return sqldb.User.destroy({ _id: req.params.id })
    .then(function () {
      res.status(204).end();
    })
    .catch(helpers.handleError(res));
}

/**
 * Change a users password
 */
exports.changePassword = function (req, res) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  return sqldb.User.findOne({
    where: {
      _id: userId
    }
  })
    .then(user => {
      if (user.authenticate(oldPass)) {
        user.password = newPass;
        return user.save()
          .then(() => {
            res.status(204).end();
          })
          .catch(helpers.validationError(res));
      } else {
        return res.status(403).end();
      }
    });
}

/**
 * set a users password
 */
exports.setPassword = function (req, res) {
  const userId = String(req.body.userId);
  const password = String(req.body.password);

  return sqldb.User.findOne({
    where: {
      _id: userId
    }
  })
    .then(user => {
      user.password = password;
      return user.save()
        .then(() => {
          res.status(204).end();
        })
        .catch(helpers.validationError(res));
    });
}

exports.changeMail = function (req, res) {
  var userId = req.user._id;
  var newEmail = req.body.email;
  // small hack in case of deletion request
  // because email needs to be uniqe
  if(req.body.email === ''){
    newEmail = userId
  }

  return sqldb.User.findOne({
    where: {
      _id: userId
    }
  })
    .then(user => {
      user.email = newEmail;
      return user.save()
        .then(() => {
          res.status(204).end();
        })
        .catch(helpers.validationError(res));
    });
}

/**
 * Get my info
 */
exports.me = function (req, res, next) {
  var userId = req.user._id;

  return sqldb.User.findOne({
    where: {
      _id: userId
    },
    attributes: [
      '_id',
      'name',
      'email',
      'role',
      'provider'
    ]
  })
    .then(user => { // don't ever give out the password or salt
      if (!user) {
        return res.status(401).end();
      }
      res.json(user);
    })
    .catch(err => next(err));
}

/**
 * Authentication callback
 */
exports.authCallback = function (req, res) {
  res.redirect('/');
}

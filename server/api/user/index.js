'use strict';

var Router = require('express').Router;
var controller = require('./user.controller');
var auth = require('../../auth/auth.service');

var router = Router();

router.get('/me', auth.isAuthenticated(), controller.me);
router.post('/changeMail', auth.isAuthenticated(), controller.changeMail);
router.get('/', auth.isAuthenticated(), controller.index);
router.post('/setPassword', auth.hasRole('admin'), controller.setPassword);


module.exports = router;

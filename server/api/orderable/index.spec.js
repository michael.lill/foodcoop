'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var orderableCtrlStub = {
  index: 'orderableCtrl.index',
  show: 'orderableCtrl.show',
  create: 'orderableCtrl.create',
  update: 'orderableCtrl.update',
  destroy: 'orderableCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var orderableIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './orderable.controller': orderableCtrlStub
});

describe('Orderable API Router:', function() {
  it('should return an express router instance', function() {
    orderableIndex.should.equal(routerStub);
  });

  describe('GET /api/orderables', function() {
    it('should route to orderable.controller.index', function() {
      routerStub.get
        .withArgs('/:year/:cw', 'orderableCtrl.index')
        .should.have.been.calledOnce;
    });
  });

  describe('POST /api/orderables', function() {
    it('should route to orderable.controller.create', function() {
      routerStub.post
        .withArgs('/', 'orderableCtrl.create')
        .should.have.been.calledOnce;
    });
  });

  describe('PUT /api/orderables/:id', function() {
    it('should route to orderable.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'orderableCtrl.update')
        .should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/orderables/:id', function() {
    it('should route to orderable.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'orderableCtrl.update')
        .should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/orderables/:id', function() {
    it('should route to orderable.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'orderableCtrl.destroy')
        .should.have.been.calledOnce;
    });
  });
});

'use strict';

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Orderable', {
    _id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    supplier: DataTypes.STRING,
    cw: DataTypes.INTEGER,
    year: DataTypes.INTEGER,
    name: DataTypes.STRING,
    price: DataTypes.FLOAT
  });
}

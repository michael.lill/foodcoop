/**
 * Orderable model events
 */

'use strict';

var EventEmitter = require('events').EventEmitter;
var Orderable = require('../../sqldb').Orderable;
var OrderableEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
OrderableEvents.setMaxListeners(0);

// Model events
var events = {
  afterCreate: 'save',
  afterUpdate: 'save',
  afterDestroy: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  var event = events[e];
  Orderable.hook(e, emitEvent(event));
}

function emitEvent(ev) {
  return function(doc, options, done) {
    OrderableEvents.emit(`${ev}:${doc._id}`, doc);
    OrderableEvents.emit(ev, doc);
    done(null);
  };
}

module.exports = OrderableEvents;

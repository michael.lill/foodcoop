/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/orderables              ->  index
 * POST    /api/orderables              ->  create
 * GET     /api/orderables/:id          ->  show
 * PUT     /api/orderables/:id          ->  update
 * DELETE  /api/orderables/:id          ->  destroy
 */

'use strict';

var sqldb = require('../../sqldb');
var helpers = require('../helpers');

// Gets a list of Orderables
exports.index = function(req, res) {
  return sqldb.Orderable.findAll({
    where: {
      year: parseInt(req.params.year),
      cw: parseInt(req.params.cw)
    },
    order: [['createdAt', 'ASC']]
  })
    .then(helpers.respondWithResult(res))
    .catch(helpers.handleError(res));
}

// Gets a single Orderable from the DB
exports.show = function(req, res) {
  return sqldb.Orderable.findOne({
    where: {
      _id: req.params.id
    }
  })
    .then(helpers.handleEntityNotFound(res))
    .then(helpers.respondWithResult(res))
    .catch(helpers.handleError(res));
}

// Creates a new Orderable in the DB
exports.create = function(req, res) {
  return sqldb.Orderable.create(req.body)
              .then(helpers.respondWithResult(res, 201))
              .catch(helpers.handleError(res));
}

// Updates an existing Orderable in the DB
exports.update = function(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return sqldb.Orderable.findOne({
    where: {
      _id: req.params.id
    }
  })
    .then(helpers.handleEntityNotFound(res))
    .then(helpers.saveUpdates(req.body))
    .then(helpers.respondWithResult(res))
    .catch(helpers.handleError(res));
}

// Deletes a Orderable from the DB
exports.destroy = function(req, res) {
  return sqldb.Product.findAll({
    include: [
      { model: sqldb.User, attributes: ['name'] }
    ],
    where: {
      OrderableId: req.params.id
    }
  }).then(products => {
    if(products.length !== 0) {
      return helpers.handleError(res, 403)('Kann nicht gelöscht werden, da folgende Bestellungen verknüpft sind: '
        + products.map(p => p.User.name).toString());
    }
    req.body.price = 0;
    return sqldb.Orderable.findOne({
      where: {
        _id: req.params.id
      }
    })
      .then(helpers.handleEntityNotFound(res))
      .then(helpers.removeEntity(res))
      .catch(helpers.handleError(res));
  });
}

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/orderables              ->  index
 * POST    /api/orderables              ->  create
 * GET     /api/orderables/:id          ->  show
 * PUT     /api/orderables/:id          ->  update
 * DELETE  /api/orderables/:id          ->  destroy
 */

'use strict';

import {Orderable, Product, User} from '../../sqldb';
import { respondWithResult, handleError, handleEntityNotFound, saveUpdates, removeEntity } from '../helpers';

// Gets a list of Orderables
export function index(req, res) {
  return Orderable.findAll({
    where: {
      year: parseInt(req.params.year),
      cw: parseInt(req.params.cw)
    }
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Orderable from the DB
export function show(req, res) {
  return Orderable.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

function createIfNotFound(req, res) {
  return function(entity) {
    if(!entity) {
      return Orderable.create(req.body)
        .then(respondWithResult(res, 201))
        .catch(handleError(res));
    }
    return respondWithResult(res)(entity);
  };
}

// Creates a new Orderable in the DB
export function create(req, res) {
  return Orderable.find({
    where: {
      supplier: req.body.supplier,
      cw: req.body.cw,
      year: req.body.year,
      name: req.body.name,
      price: req.body.price
    }
  }).then(createIfNotFound(req, res));
}

function updatePrices(req) {
  Product.findAll({
    where: {
      OrderableId: req.params.id
    }
  }).then(function(result) {
    for(let oldEntity of result) {
      let newEntity = {};
      newEntity._id = oldEntity._id;
      newEntity.pricePerQuantity = req.body.price;
      newEntity.price = newEntity.pricePerQuantity * oldEntity.quantity;
      saveUpdates(newEntity)(oldEntity);
    }
  });
  return function(entity) {
    return entity;
  };
}

// Updates an existing Orderable in the DB
export function update(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Orderable.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(updatePrices(req))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Orderable from the DB
export function destroy(req, res) {
  return Product.findAll({
    include: [
      { model: User, attributes: ['name'] }
    ],
    where: {
      OrderableId: req.params.id
    }
  }).then(products => {
    if(products.length !== 0) {
      return handleError(res, 403)('Kann nicht gelöscht werden, da folgende Bestellungen verknüpft sind: '
        + products.map(p => p.User.name).toString());
    }
    req.body.price = 0;
    return Orderable.find({
      where: {
        _id: req.params.id
      }
    })
      .then(updatePrices(req))
      .then(handleEntityNotFound(res))
      .then(removeEntity(res))
      .catch(handleError(res));
  });
}

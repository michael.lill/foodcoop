'use strict';

var express = require('express');
var router = express.Router();

var sqldb = require('../../sqldb');
var helpers = require('../helpers');

// Gets a list of Settings
exports.index = function(req, res) {
  return sqldb.Setting.findAll({
      where: {
        key: req.params.key
      }
    })
    .then(helpers.respondWithResult(res))
    .catch(helpers.handleError(res));
}

// Gets a single Setting from the DB
exports.show = function(req, res) {
  return sqldb.Setting.findOne({
      where: {
        _id: req.params.id
      }
    })
    .then(helpers.handleEntityNotFound(res))
    .then(helpers.respondWithResult(res))
    .catch(helpers.handleError(res));
}

function preSave(req) {
  var newEntity = req.body;
  return newEntity;
}

// Creates a new Setting in the DB
exports.create = function(req, res) {
  return sqldb.Setting.create(preSave(req))
    .then(helpers.respondWithResult(res, 201))
    .catch(helpers.handleError(res));
}

// Updates an existing Setting in the DB
exports.update = function(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return sqldb.Setting.findOne({
      where: {
        _id: req.params.id,
      }
    })
    .then(helpers.handleEntityNotFound(res))
    .then(helpers.saveUpdates(preSave(req)))
    .then(helpers.respondWithResult(res))
    .catch(helpers.handleError(res));
}

// Deletes a Setting from the DB
exports.destroy = function(req, res) {
  return sqldb.Setting.findOne({
      where: {
        _id: req.params.id
      }
    })
    .then(helpers.handleEntityNotFound(res))
    .then(helpers.saveUpdates({
      deleted: true
    }))
    .then(() => {
      res.status(204).end();
    })
    .catch(helpers.handleError(res));
}


router.get('/:key', exports.index);
router.post('/', exports.create);
router.put('/:id', exports.update);
router.patch('/:id', exports.update);
router.delete('/:id', exports.destroy);

module.exports = router;

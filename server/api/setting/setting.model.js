'use strict';

export default function(sequelize, DataTypes) {
  return sequelize.define('Setting', {
    _id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    key: DataTypes.STRING,
    value: DataTypes.STRING
  });
}
'use strict';

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Message', {
    _id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    text: DataTypes.STRING
  });
}

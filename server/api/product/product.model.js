'use strict';

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Product', {
    _id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: DataTypes.STRING,
    quantity: DataTypes.FLOAT,
    pricePerQuantity: DataTypes.FLOAT,
    cw: DataTypes.INTEGER,
    year: DataTypes.INTEGER,
    deleted: DataTypes.BOOLEAN
  });
}

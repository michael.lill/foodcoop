'use strict';

var express = require('express');
var controller = require('./product.controller');

var router = express.Router();

router.get('/:year/:cw', controller.index);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/products              ->  index
 * POST    /api/products              ->  create
 * GET     /api/products/:id          ->  show
 * PUT     /api/products/:id          ->  update
 * DELETE  /api/products/:id          ->  destroy
 */

'use strict';

var sqldb = require('../../sqldb');
var helpers = require('../helpers');

const morphProducts = helpers.morphProducts;
const morphProduct = helpers.morphProduct;
const select = helpers.select(sqldb);


// Gets a list of Products
exports.index = function (req, res) {
  const year = parseInt(req.params.year);
  const cw = parseInt(req.params.cw);
  const userId = parseInt(req.user._id);
  return select(`SELECT "Product"."_id", "Product"."name", "Product"."quantity", "Product"."pricePerQuantity", "Product"."cw", "Product"."year", "Product"."deleted", "Product"."createdAt", "Product"."updatedAt", "Product"."OrderableId", "Product"."UserId", "product_total"."_id" AS "product_total._id", "product_total"."total" AS "product_total.total" FROM "Products" AS "Product" INNER JOIN "product_total" AS "product_total" ON "Product"."_id" = "product_total"."_id" WHERE "Product"."deleted" = false AND "Product"."year" = ${year} AND "Product"."cw" = ${cw} AND "Product"."UserId" = '${userId}' ORDER BY "Product"."createdAt";`)
    .then(helpers.spreadQueryResult)
    .then(morphProducts)
    .then(helpers.respondWithResult(res))
    .catch(helpers.handleError(res));
}

// Gets a single Product from the DB
exports.show = function (req, res) {
  const productId = parseInt(req.params.id);
  return select(`SELECT "Product"."_id", "Product"."name", "Product"."quantity", "Product"."pricePerQuantity", "Product"."cw", "Product"."year", "Product"."deleted", "Product"."createdAt", "Product"."updatedAt", "Product"."OrderableId", "Product"."UserId", "product_total"."_id" AS "product_total._id", "product_total"."total" AS "product_total.total" FROM "Products" AS "Product" INNER JOIN "product_total" AS "product_total" ON "Product"."_id" = "product_total"."_id" WHERE "Product"."deleted" = false AND "Product"."_id" = '${productId}';`)
    .then(helpers.spreadQueryResult)
    .then(queryResult => queryResult[0])
    .then(helpers.handleEntityNotFound(res))
    .then(morphProduct)
    .then(helpers.respondWithResult(res))
    .catch(helpers.handleError(res));
}

function preSave(req) {
  var newEntity = req.body;
  newEntity.UserId = req.user._id;
  return newEntity;
}

// Creates a new Product in the DB
exports.create = async function (req, res) {
  let product = await sqldb.Product.create(preSave(req));
  req.params.id = product._id;
  return await exports.show(req, res);
}

// Updates an existing Product in the DB
exports.update = async function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }

  await sqldb.Product.findOne({
    where: {
      deleted: false,
      _id: req.params.id,
      UserId: req.user._id
    }
  })
    .then(helpers.handleEntityNotFound(res))
    .then(helpers.saveUpdates(preSave(req)));
  return await exports.show(req, res);
}

// Deletes a Product from the DB
exports.destroy = function (req, res) {
  return sqldb.Product.findOne({
    where: {
      _id: req.params.id,
      UserId: req.user._id
    }
  })
    .then(helpers.handleEntityNotFound(res))
    .then(helpers.saveUpdates({ deleted: true }))
    .then(() => {
      res.status(204).end();
    })
    .catch(helpers.handleError(res));
}

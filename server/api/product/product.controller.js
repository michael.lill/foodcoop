/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/products              ->  index
 * POST    /api/products              ->  create
 * GET     /api/products/:id          ->  show
 * PUT     /api/products/:id          ->  update
 * DELETE  /api/products/:id          ->  destroy
 */

'use strict';

import { Product } from '../../sqldb';
import { respondWithResult, handleError, handleEntityNotFound, saveUpdates } from '../helpers';

// Gets a list of Products
export function index(req, res) {
  return Product.findAll({
    where: {
      deleted: false,
      year: parseInt(req.params.year),
      cw: parseInt(req.params.cw),
      UserId: req.user._id
    }
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Product from the DB
export function show(req, res) {
  return Product.find({
    where: {
      deleted: false,
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

function preSave(req) {
  var newEntity = req.body;
  newEntity.price = newEntity.pricePerQuantity * newEntity.quantity;
  newEntity.UserId = req.user._id;
  return newEntity;
}

// Creates a new Product in the DB
export function create(req, res) {
  return Product.create(preSave(req))
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing Product in the DB
export function update(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Product.find({
    where: {
      deleted: false,
      _id: req.params.id,
      UserId: req.user._id
    }
  })
    .then(handleEntityNotFound(res))
    .then(saveUpdates(preSave(req)))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Product from the DB
export function destroy(req, res) {
  return Product.find({
    where: {
      _id: req.params.id,
      UserId: req.user._id
    }
  })
    .then(handleEntityNotFound(res))
    .then(saveUpdates({ deleted: true }))
    .then(() => {
      res.status(204).end();
    })
    .catch(handleError(res));
}

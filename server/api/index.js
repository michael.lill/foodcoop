import { Product, Orderable, User, sequelize } from '../sqldb';
import { sendMail } from '../mailer';
import { respondWithResult, handleError } from './helpers';
import settings from '../config/local.env';

var express = require('express');
var router = express.Router();

router.get('/notificationstatus/:year/:cw', function (req, res) {
  return respondWithResult(res)({ status: false });
});

var getUsersWhoWantToBeNotified = function () {
  return User
    .findAll({
      where: {
        email: {
          $like: '%@%'
        }
      }
    })
    .then(users => users.map(user => user.email));
};

const icsTemplate = `BEGIN:VEVENT
SUMMARY:Gemüse abholen
DTSTART;TZID=Europe/Berlin:{{DATE}}T183000
DTEND;TZID=Europe/Berlin:{{DATE}}T203000
LOCATION:FoodCoop Karlsruhe\\, Marie-Alexandra-Str. 34\\, 76135 Karlsruhe
BEGIN:VALARM
ACTION:DISPLAY
TRIGGER;VALUE=DURATION:-PT1H
DESCRIPTION:Gemüseabholung beginnt in einer Stunde
END:VALARM
END:VEVENT`;

router.get('/sendnotifications/:year/:cw', function (req, res) {
  const wednesday = new Date((new Date(req.params.year, 0, 4)).getTime() - ((new Date(req.params.year, 0, 4)).getDay() - 4 - (parseInt(req.params.cw) - 1) * 7) * 24 * 60 * 60 * 1000);
  const date = wednesday
    .toISOString()
    .replace('-', '')
    .replace('-', '')
    .substring(0, 8);
  const ics = icsTemplate
    .replace('{{DATE}}', date)
    .replace('{{DATE}}', date);

  getUsersWhoWantToBeNotified().then(mailAdresses => {
    sendMail(mailAdresses.join(','),
      `Bestellungen für KW ${req.params.cw} jetzt möglich ✔`,
      '<a href="https://foodcoop-bestellung.de/">Hier klicken</a><br>'
      + '<br>'
      + 'Diese Mail wurde von der Foodcoopbestellplattform verschickt.<br>'
      + 'Wenn du dich abmelden möchtest, lösche deine E-Mailadresse unter Hilfe -> "Kann ich mich benachrichtigen lassen..."',
      {
        filename: 'Abholungstermin.ics',
        content: new Buffer(ics, 'utf-8')
      },
      (error, sentMailInfo) => {
        if (error) {
          return handleError(res);
        }
        return respondWithResult(res)({
          usersNotified: mailAdresses.length,
          result: sentMailInfo
        });
      }
    );
  }, handleError);
});

router.get('/orderDetails/:year/:cw', function (req, res) {
  var year = parseInt(req.params.year);
  var cw = parseInt(req.params.cw);
  Product
    .findAll({
      include: [
        { model: User, attributes: ['name'] },
        { model: Orderable, attributes: ['supplier'] }
      ],
      where: {
        deleted: false,
        $or: [
          { year, cw, OrderableId: { $ne: null }, quantity: { $gt: 0 } },
          { year, cw, name: 'wednesday' }
        ]
      }
    })
    .then(respondWithResult(res))
    .catch(handleError(res));
});

router.get('/transactions', function (req, res) {
  return Product
    .findAll({
      where: {
        deleted: false,
        UserId: req.user._id,
        price: { $ne: 0 },
        $or: [{ name: 'rent' }, { name: 'deposit' }]
      }
    })
    .then(respondWithResult(res))
    .catch(handleError(res));
});

router.get('/balance', function (req, res) {
  Product
    .findAll({
      where: {
        deleted: false,
      },
      attributes: [[sequelize.fn('SUM', sequelize.col('price')), 'balance']],
    })
    .then(result => {
      let balance = -result[0].dataValues.balance;
      return balance === null || balance === 0
        ? respondWithResult(res)(0.00000000000001)
        : respondWithResult(res)(balance);
    })
    .catch(handleError(res));
});

router.get('/startBalance/:year/:cw', function (req, res) {
  var year = parseInt(req.params.year);
  var cw = parseInt(req.params.cw);
  Product
    .findAll({
      where: {
        deleted: false,
        UserId: req.user._id,
        $or: [{ $and: [{ year }, { cw: { $lt: cw } }] }, { year: { $lt: year } }]
      },
      attributes: [[sequelize.fn('SUM', sequelize.col('price')), 'balance']],
    })
    .then(result => {
      let balance = -result[0].dataValues.balance;
      return balance === null || balance === 0
        ? respondWithResult(res)(0.00000000000001)
        : respondWithResult(res)(balance);
    })
    .catch(handleError(res));
});

router.get('/balances/:year/:cw', function (req, res) {
  var year = parseInt(req.params.year);
  var cw = parseInt(req.params.cw);
  Product
    .findAll({
      include: [
        { model: User, attributes: ['name'] },
      ],
      where: {
        deleted: false,
        $or: [{ $and: [{ year }, { cw: { $lte: cw } }] }, { year: { $lt: year } }]
      },
      group: ['UserId'],
      attributes: [[sequelize.fn('SUM', sequelize.col('price')), 'balance']],
    })
    .then(respondWithResult(res))
    .catch(handleError(res));
});

router.get(
  '/inventory/:startyear/:startcw/:endyear/:endcw', function (req, res) {
    var startyear = parseInt(req.params.startyear);
    var startcw = parseInt(req.params.startcw);
    var endyear = parseInt(req.params.endyear);
    var endcw = parseInt(req.params.endcw);
    Product
      .findAll({
        include: [
          { model: User, attributes: ['name'] },
          { model: Orderable, attributes: ['supplier'] }
        ],
        where: {

          price: { $ne: 0 },
          deleted: false,

          $and: [
            { year: { $gte: startyear } },
            { year: { $lte: endyear } },
            {
              $or: [{ year: { $ne: startyear } }, { cw: { $gte: startcw } }],
            },
            {
              $or: [{ year: { $ne: endyear } }, { cw: { $lte: endcw } }]
            }
          ]
        }


      })
      .then(respondWithResult(res))
      .catch(handleError(res));
  });


router.get('/allMyProducts', function (req, res) {
  Product
    .findAll({
      include: [
        { model: User, attributes: ['name'] },
        { model: Orderable, attributes: ['supplier'] }
      ],
      where: {
        UserId: req.user._id,
        price: { $ne: 0 },
        deleted: false
      }
    })
    .then(respondWithResult(res))
    .catch(handleError(res));
});

router.get('/testmail', function (req, res) {
  var userId = req.user._id;
  return User.find({
    where: {
      _id: userId
    }
  }).then(user => {
    sendMail(user.email, 'Testmail', 'Alles gut. Deine Mailadresse funktioniert.', false, (error, sentMailInfo) => {
      error !== null ? handleError(res)(error) : respondWithResult(res)(sentMailInfo);
    });
  });
});

router.get('/recentProducts', function (req, res) {
  return Product.findAll({
    where: {
      pricePerQuantity: { $ne: 0 },
      quantity: { $ne: 0 },
      deleted: false,
      OrderableId: null,
      $and: [
        { name: { $notLike: 'rent' } },
        { name: { $notLike: 'deposit' } },
        { name: { $notLike: 'wednesday' } }
      ]
    },
    order: [
      ['year', 'DESC'],
      ['cw', 'DESC']
    ],
    limit: 400,
    attributes: ['name', 'pricePerQuantity']
  }).then(orderables => {
    let result = [];
    for (const orderable of orderables) {
      if (result.every(r => r.name !== orderable.name)) {
        result.push(orderable);
      }
    }
    respondWithResult(res)(result);
  });
});

router.post('/db', function (req, res) {
  if (req.body.password === settings.SQLPW) {
    try {
      sequelize.query(req.body.sql).spread((results, metadata) => {
        respondWithResult(res)({
          results: results,
          metadata: metadata
        });
      });
      return;
    } catch (e) {
      handleError(res)(e);
      return;
    }
  }
  handleError(res, 401)('Falsches Passwort.');
});

module.exports = router;

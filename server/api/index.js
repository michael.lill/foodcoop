var mailer = require('../mailer');
var sendMail = mailer.sendMail;
var sqldb = require('../sqldb');
var helpers = require('./helpers');
var settings = require('../config/local.env');

var express = require('express');
var router = express.Router();

const Op = sqldb.Sequelize.Op;

const morphProducts = helpers.morphProducts;
const morphProduct = helpers.morphProduct;
const select = helpers.select(sqldb);

var getUsersWhoWantToBeNotified = function () {
  return sqldb.User
    .findAll({
      where: {
        email: {
          [Op.like]: '%@%'
        }
      }
    })
    .then(users => users.map(user => user.email));
};

const icsTemplate = `BEGIN:VEVENT
SUMMARY:Gemüse abholen
DTSTART;TZID=Europe/Berlin:{{DATE}}T183000
DTEND;TZID=Europe/Berlin:{{DATE}}T203000
LOCATION:FoodCoop Karlsruhe\\, Marie-Alexandra-Str. 34\\, 76135 Karlsruhe
BEGIN:VALARM
ACTION:DISPLAY
TRIGGER;VALUE=DURATION:-PT1H
DESCRIPTION:Gemüseabholung beginnt in einer Stunde
END:VALARM
END:VEVENT`;

router.get('/sendnotifications/:year/:cw', async function (req, res) {
  const wednesday = new Date((new Date(req.params.year, 0, 4)).getTime() - ((new Date(req.params.year, 0, 4)).getDay() - 3 - (parseInt(req.params.cw) - 1) * 7) * 24 * 60 * 60 * 1000);
  const date = wednesday
    .toISOString()
    .replace('-', '')
    .replace('-', '')
    .substring(0, 8);
  const ics = icsTemplate
    .replace('{{DATE}}', date)
    .replace('{{DATE}}', date);

  const mailAdresses = await getUsersWhoWantToBeNotified();
  try {
    const sentMailInfo = await sendMail(mailAdresses.join(','),
      `Bestellungen für KW ${req.params.cw} jetzt möglich ✔`,
      '<a href="https://bestellung.lill.ovh/">Hier klicken</a><br>'
      + '<br>'
      + 'Diese Mail wurde von der Foodcoopbestellplattform verschickt.<br>'
      + 'Wenn du dich abmelden möchtest, lösche deine E-Mailadresse unter Hilfe -> "Kann ich mich benachrichtigen lassen..."',
      {
        filename: 'Abholungstermin.ics',
        content: Buffer.from(ics, 'utf-8')
      }
    );
    return helpers.respondWithResult(res)({
      usersNotified: mailAdresses.length,
      result: sentMailInfo
    });
  } catch (error) {
    return helpers.handleError(res);
  }
});

router.get('/orderDetails/:year/:cw', function (req, res) {
  var year = parseInt(req.params.year);
  var cw = parseInt(req.params.cw);
  select(`SELECT "Product"."_id", "Product"."name", "Product"."quantity", "Product"."pricePerQuantity", "Product"."cw", "Product"."year", "Product"."deleted", "Product"."createdAt", "Product"."updatedAt", "Product"."OrderableId", "Product"."UserId", "User"."_id" AS "User._id", "User"."name" AS "User.name", "Orderable"."_id" AS "Orderable._id", "Orderable"."supplier" AS "Orderable.supplier", "product_total"."_id" AS "product_total._id", "product_total"."total" AS "product_total.total" FROM "Products" AS "Product" LEFT OUTER JOIN "Users" AS "User" ON "Product"."UserId" = "User"."_id" LEFT OUTER JOIN "Orderables" AS "Orderable" ON "Product"."OrderableId" = "Orderable"."_id" INNER JOIN "product_total" AS "product_total" ON "Product"."_id" = "product_total"."_id" WHERE (("Product"."year" = ${year} AND "Product"."cw" = ${cw} AND "Product"."OrderableId" IS NOT NULL AND "Product"."quantity" > '0') OR ("Product"."year" = ${year} AND "Product"."cw" = ${cw} AND "Product"."name" = 'wednesday')) AND "Product"."deleted" = false ORDER BY "Product"."createdAt";`)
    .then(helpers.spreadQueryResult)
    .then(morphProducts)
    .then(helpers.respondWithResult(res))
    .catch(helpers.handleError(res));
});

router.get('/transactions', function (req, res) {
  const userId = parseInt(req.user._id);
  return select(`SELECT "Product"."_id", "Product"."name", "Product"."quantity", "Product"."pricePerQuantity", "Product"."cw", "Product"."year", "Product"."deleted", "Product"."createdAt", "Product"."updatedAt", "Product"."OrderableId", "Product"."UserId", "product_total"."_id" AS "product_total._id", "product_total"."total" AS "product_total.total" FROM "Products" AS "Product" INNER JOIN "product_total" AS "product_total" ON "Product"."_id" = "product_total"."_id" WHERE ("Product"."name" = 'rent' OR "Product"."name" = 'deposit') AND "Product"."deleted" = false AND "Product"."UserId" = '${userId}' AND "product_total"."total" != 0 ORDER BY "Product"."year","Product"."cw";`)
    .then(helpers.spreadQueryResult)
    .then(morphProducts)
    .then(helpers.respondWithResult(res))
    .catch(helpers.handleError(res));
});

router.get('/balance', function (req, res) {
  select(`SELECT SUM("product_total"."total") AS "balance" FROM "Products" AS "Product" INNER JOIN "product_total" AS "product_total" ON "Product"."_id" = "product_total"."_id" WHERE "Product"."deleted" = false;`)
    .then(helpers.spreadQueryResult)
    .then(morphProducts)
    .then(result => {
      let balance = -result[0].balance;
      return balance === null || balance === 0
        ? helpers.respondWithResult(res)(0.00000000000001)
        : helpers.respondWithResult(res)(balance);
    })
    .catch(helpers.handleError(res));
});

router.get('/startBalance/:year/:cw', function (req, res) {
  const userId = parseInt(req.user._id)
  const year = parseInt(req.params.year);
  const cw = parseInt(req.params.cw);
  select(`SELECT SUM("product_total"."total") AS "balance" FROM "Products" INNER JOIN "product_total" ON "Products"."_id" = "product_total"."_id" WHERE (("Products"."year" = ${year} AND "Products"."cw" < ${cw}) OR "Products"."year" < ${year}) AND "Products"."deleted" = false AND "Products"."UserId" = ${userId};`)
    .then(result => {
      let balance = -result[0].balance;
      return balance === null || balance === 0
        ? helpers.respondWithResult(res)(0.00000000000001)
        : helpers.respondWithResult(res)(balance);
    })
    .catch(helpers.handleError(res));
});

router.get('/balances/:year/:cw', function (req, res) {
  var year = parseInt(req.params.year);
  var cw = parseInt(req.params.cw);
  select(`SELECT SUM("product_total"."total") AS "balance", "User"."name" AS "User.name" FROM "Products" AS "Product" LEFT OUTER JOIN "Users" AS "User" ON "Product"."UserId" = "User"."_id" INNER JOIN "product_total" AS "product_total" ON "Product"."_id" = "product_total"."_id" WHERE (("Product"."year" = ${year} AND "Product"."cw" <= ${cw}) OR "Product"."year" < ${year}) AND "Product"."deleted" = false GROUP BY "User.name";`)
    .then(helpers.spreadQueryResult)
    .then(helpers.respondWithResult(res))
    .catch(helpers.handleError(res));
});

router.get(
  '/inventory/:startyear/:startcw/:endyear/:endcw', function (req, res) {
    var startyear = parseInt(req.params.startyear);
    var startcw = parseInt(req.params.startcw);
    var endyear = parseInt(req.params.endyear);
    var endcw = parseInt(req.params.endcw);
    select(`SELECT "Product"."_id", "Product"."name", "Product"."quantity", "Product"."pricePerQuantity", "Product"."cw", "Product"."year", "Product"."deleted", "Product"."createdAt", "Product"."updatedAt", "Product"."OrderableId", "Product"."UserId", "User"."_id" AS "User._id", "User"."name" AS "User.name", "Orderable"."_id" AS "Orderable._id", "Orderable"."supplier" AS "Orderable.supplier", "product_total"."_id" AS "product_total._id", "product_total"."total" AS "product_total.total" FROM "Products" AS "Product" LEFT OUTER JOIN "Users" AS "User" ON "Product"."UserId" = "User"."_id" LEFT OUTER JOIN "Orderables" AS "Orderable" ON "Product"."OrderableId" = "Orderable"."_id" INNER JOIN "product_total" AS "product_total" ON "Product"."_id" = "product_total"."_id" WHERE ("Product"."year" >= ${startyear} AND "Product"."year" <= ${endyear} AND ("Product"."year" != ${startyear} OR "Product"."cw" >= ${startcw}) AND ("Product"."year" != ${endyear} OR "Product"."cw" <= ${endcw})) AND "product_total"."total" != 0 AND "Product"."deleted" = false ORDER BY "Product"."createdAt";`)
      .then(helpers.spreadQueryResult)
      .then(morphProducts)
      .then(helpers.respondWithResult(res))
      .catch(helpers.handleError(res));
  });


router.get('/allMyProducts', function (req, res) {
  const userId = parseInt(req.user._id);
  select(`SELECT "Product"."_id", "Product"."name", "Product"."quantity", "Product"."pricePerQuantity", "Product"."cw", "Product"."year", "Product"."deleted", "Product"."createdAt", "Product"."updatedAt", "Product"."OrderableId", "Product"."UserId", "User"."_id" AS "User._id", "User"."name" AS "User.name", "Orderable"."_id" AS "Orderable._id", "Orderable"."supplier" AS "Orderable.supplier", "product_total"."_id" AS "product_total._id", "product_total"."total" AS "product_total.total" FROM "Products" AS "Product" LEFT OUTER JOIN "Users" AS "User" ON "Product"."UserId" = "User"."_id" LEFT OUTER JOIN "Orderables" AS "Orderable" ON "Product"."OrderableId" = "Orderable"."_id" INNER JOIN "product_total" AS "product_total" ON "Product"."_id" = "product_total"."_id" WHERE "Product"."UserId" = '${userId}' AND "product_total"."total" != 0 AND "Product"."deleted" = false ORDER BY "Product"."createdAt";`)
    .then(helpers.spreadQueryResult)
    .then(morphProducts)
    .then(helpers.respondWithResult(res))
    .catch(helpers.handleError(res));
});

router.get('/testmail', async function (req, res) {
  var userId = req.user._id;
  const user = await sqldb.User.findOne({
    where: {
      _id: userId
    }
  });
  try {
    const sentMailInfo = await sendMail(user.email, 'Testmail', 'Alles gut. Deine Mailadresse funktioniert.', false);
    return helpers.respondWithResult(res)(sentMailInfo);
  } catch (error) {
    return helpers.handleError(res)(error)
  }
});

function recentProductsSQLQuery(currentYear) {
  return `
      select name, "pricePerQuantity", "updatedAt" from (
          SELECT
            ROW_NUMBER () OVER (
                PARTITION BY name
                order by year desc, cw desc
            ) as "RowNum",
            name,
            "pricePerQuantity",
            "updatedAt",
            year,
            cw
            FROM
                "Products", product_total
            where
              "Products"._id = product_total._id
            and
              "pricePerQuantity" >= 0.01
            and
              quantity >= 0.01
            and
              total >= 0.01
            and 
              "deleted" = false
            and 
              "OrderableId" is null
            and 
              name != 'rent'
            and 
              name != 'deposit'
            and 
              name != 'wednesday'
            and 
              name NOT IN (SELECT CONCAT(name, '♦',unit,'♦k.A.♦k.A.♦',supplier) FROM "Nonperishables")
            and
              year >= ${currentYear - 1}
          ) as "byName"
          where "RowNum" = 1
          order by year desc, cw desc
          limit 250
      `;
}

// TODO cleanup naming of route
router.get('/recentProducts', async function (req, res) {
  const currentYear = (new Date()).getFullYear();
  const orderables = (await select(recentProductsSQLQuery(currentYear)))
    .map(orderable => {
      if (orderable.updatedAt && orderable.updatedAt.length === 30) {
        return {
          name: orderable.name,
          pricePerQuantity: orderable.pricePerQuantity,
          updatedAt: orderable.updatedAt.substring(8, 10) + "."
            + orderable.updatedAt.substring(5, 7)
            + "." + orderable.updatedAt.substring(0, 4),
          flavor: 'Orderable'
        };
      }
      return {
        ...orderable,
        flavor: 'Orderable'
      };
    });
  const nonperishables = (await select(`SELECT "supplier", "name", "pricePerQuantity", "unit" FROM "Nonperishables" WHERE "name" != '' AND "pricePerQuantity" > 0`))
    .map(nonperishable => {
      return {
        name: `${nonperishable.name}♦${nonperishable.unit ? nonperishable.unit : 'k.A.'}♦k.A.♦k.A.♦${nonperishable.supplier ? nonperishable.supplier : 'k.A.'}`,
        pricePerQuantity: nonperishable.pricePerQuantity,
        flavor: 'Nonperishable'
      }
    });
  return helpers.respondWithResult(res)([
    ...nonperishables,
    ...orderables
  ]);
});

router.get('/suppliers', function (req, res) {
  return select('select distinct "supplier" from "Orderables" where "_id" in (select "OrderableId" from "Products" where "deleted" = false and "OrderableId" is not null order by "year" desc, "cw" desc limit 5000) order by "supplier"')
    .then(result => {
      helpers.respondWithResult(res)(result);
    });
});

router.get('/turnover', function (req, res) {
  const currentYear = (new Date()).getFullYear();
  return select(`select year,cw,sum(total) as turnover from "Products", product_total where "Products"._id = product_total._id and "deleted" = false and name != 'deposit' and name != 'rent' and total > 0 and year >= ${currentYear - 2} group by year,cw order by year,cw`)
    .then(result => {
      helpers.respondWithResult(res)(result);
    });
});

module.exports = router;

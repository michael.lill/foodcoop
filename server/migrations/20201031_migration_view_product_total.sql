CREATE VIEW product_total AS
    SELECT products._id as _id, pricePerQuantity * quantity as total
        FROM products where products.OrderableId is null
union 
    SELECT products._id as _id, Orderables.price * quantity as total
        FROM products,Orderables where products.OrderableId is not null and Orderables._id = Products.OrderableId
DROP VIEW product_total;
CREATE VIEW product_total AS
    SELECT "Products"._id as _id, "Products"."pricePerQuantity" * "Products".quantity as total
        FROM "Products" where "Products"."OrderableId" is null
UNION ALL
    SELECT "Products"._id as _id, "Orderables".price * quantity as total
        FROM "Products","Orderables" where "Products"."OrderableId" is not null and "Orderables"._id = "Products"."OrderableId";
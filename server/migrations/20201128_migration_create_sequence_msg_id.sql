CREATE SEQUENCE "Messages__id_seq" START 1;
ALTER TABLE "Messages" ALTER COLUMN _id SET DEFAULT nextval('"Messages__id_seq"'::regclass);

CREATE INDEX `index_orderables_year_cw` ON `Orderables` (
	`year`	DESC,
    `cw`	DESC
);

CREATE INDEX `index_products_year_cw` ON `Products` (
	`year`	DESC,
	`cw`	DESC
);

CREATE INDEX `index_products_userid` ON `Products` (
	`UserId` DESC
);

CREATE INDEX `index_products_orderableid` ON `Products` (
	`OrderableId` DESC
);
/**
 * Main application routes
 */

'use strict';

import errors from './components/errors';
import path from 'path';
import {
  isAuthenticated
} from './auth/auth.service';

export default function (app) {
  if (process.env.NODE_ENV !== 'test') {
    app.use('/api/*', isAuthenticated());
  }
  // Insert routes below
  app.use('/api', require('./api'));
  app.use('/api/orderables', require('./api/orderable'));
  app.use('/api/products', require('./api/product'));
  app.use('/api/users', require('./api/user'));
  app.use('/api/settings', require('./api/setting'));

  app.use('/auth', require('./auth'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|node_modules|assets)/*')
    .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*').get((req, res) => {
    if (req.headers['x-forwarded-proto'] === 'http') {
      res.redirect(`https://${req.headers.host}${req.path}`);
    } else {
      res.sendFile(path.resolve(`${app.get('appPath')}/index.html`));
    }
  });
}

'use strict';

// Test specific configuration
// ===========================
module.exports = {
  sequelize: {
    uri: process.env.SEQUELIZE_URI,
    options: {
      logging: false,
      define: {
        timestamps: false
      }
    }
  }
};

alter table Orderables add column createdAt;
alter table Orderables add column updatedAt;
alter table Products add column createdAt;
alter table Products add column updatedAt;
alter table Sessions add column createdAt;
alter table Sessions add column updatedAt;
alter table Users add column createdAt;
alter table Users add column updatedAt;

'use strict';

// Use local.env.js for environment variables that gulp will set when the
// server starts locally.
// Use for your api keys, secrets, etc. This file should not be tracked by git.
//
// You will need to set these on the server you deploy to.

module.exports = {
  DOMAIN: 'http://localhost:9000',
  SESSION_SECRET: 'foodcoop-secret',

  // Control debug level for modules using visionmedia/debug
  DEBUG: '',
  MAIL_USER: 'someone@gmail.com',
  MAIL_PASSWORD: 'pw',
  MAIL_HOST: 'smtp.gmail.com'
};

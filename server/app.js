/**
 * Main application file
 */

'use strict';

var express = require('express');
var sqldb = require('./sqldb');
var config = require('./config/environment');
var http = require('http');
var helmet = require('helmet');
const ws = require('ws');

const wsServer = new ws.Server({ noServer: true });
wsServer.on('connection', require('./websocket')(wsServer));

// Populate databases with sample data
if (config.seedDB) {
  require('./config/seed');
}

// Setup server
var app = express();
app._ready = new Promise((resolve, reject) => {
  var server = http.createServer(app);
  require('./config/express')(app);
  require('./routes')(app);

  app.use(helmet());

  // Start server
  function startServer() {
    app.angularFullstack = server.listen(config.port, config.ip, function () {
      console.log('Express server listening on %d, in %s mode', config.port,
        app.get('env'));
      resolve();
    });
    app.angularFullstack.on('upgrade', (request, socket, head) => {
      wsServer.handleUpgrade(request, socket, head, socket => {
        wsServer.emit('connection', socket, request);
      });
    });
  }

  console.log('Invoking sequelize sync');
  sqldb.sequelize
    .sync()
    .then(startServer)
    .catch(function (err) {
      console.log('Server failed to start due to error: %s', err);
    });
});


// Expose app
exports = module.exports = app;

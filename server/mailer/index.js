const nodemailer = require('nodemailer');
const settings = require('../config/local.env');

const transporter = nodemailer.createTransport({
  host: settings.MAIL_HOST,
  port: 25,
  secure: false, // use SSL
  auth: {
    user: settings.MAIL_USER,
    pass: settings.MAIL_PASSWORD
  }
});

const sendMail = async (to, subject, html, attachment) => {
  const mailOptions = {
    from: 'noreply@foodcoop-karlsruhe.de', // sender address
    bcc: to, // list of receivers
    subject, // Subject line
    html,
  };

  if (attachment) {
    mailOptions.attachments = [attachment];
  }

  return await transporter.sendMail(mailOptions);
};

module.exports = { sendMail };

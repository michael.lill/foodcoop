var nodemailer = require('nodemailer');
import settings from '../config/local.env';

var transporter = nodemailer.createTransport({
  host: settings.MAIL_HOST,
  port: 25,
  secure: false, // use SSL
  auth: {
    user: settings.MAIL_USER,
    pass: settings.MAIL_PASSWORD
  }
});

var sendMail = (to, subject, html, attachment, callback) => {
  var mailOptions = {
    from: 'noreply@foodcoop-karlsruhe.de', // sender address
    bcc: to, // list of receivers
    subject, // Subject line
    html,
  };

  if (attachment) {
    mailOptions.attachments = [attachment];
  }

  // send mail with defined transport object
  transporter.sendMail(mailOptions, callback);
};

module.exports = { sendMail };

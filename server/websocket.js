var sqldb = require('./sqldb');

module.exports = function (wsServer) {
    return (socket, request) => {

        let resolveUser;
        const asyncUser = new Promise((resolve,reject) => {
            resolveUser = resolve;
        });

        function getCookieValueByName(cookieString, name) {
            var match = cookieString.match(new RegExp("(^| )" + name + "=([^;]+)"));
            return match ? match[2] : "";
        }

        const token = getCookieValueByName(request.headers.cookie, "token");
        request.headers.authorization = `Bearer ${token}`;

        require('./auth/auth.service')
            .isAuthenticated()(request, {}, (err) => {
                if (err) {
                    console.warn(err);
                }
                resolveUser(request.user);
            });

        socket.on('message', (async message => {
            const user = await asyncUser;
            if(!user || !user._id){
                return;
            }
            if (message === 'loadMessages') {
                sqldb.Message.findAll({
                    include: [
                        { model: sqldb.User, attributes: ['name'] },
                    ],
                    order: [['createdAt', 'ASC']]
                })
                    .then(messages => socket.send(JSON.stringify(messages)));
            } else {
                const parsedMessage = JSON.parse(message);
                sqldb.Message.create({
                    UserId: user._id,
                    text: parsedMessage.text
                }).then(msg => broadcastNewMessage(msg._id, wsServer))
            }
        }));
    };
}

async function broadcastNewMessage(messageId, wsServer) {
    let message = await readNewMessage(messageId);
    wsServer.clients.forEach(function each(client) {
        const OPEN = 1;
        if (client.readyState === OPEN) {
            client.send(JSON.stringify([message]));
        }
    });
}

async function readNewMessage(messageId) {
    return await sqldb.Message.findOne({
        include: [
            { model: sqldb.User, attributes: ['name'] },
        ],
        where: {
            _id: messageId
        }
    });
}

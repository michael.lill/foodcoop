'use strict';

var config = browser.params;

describe('Main View', function() {
  var page;

  beforeEach(function() {
    browser.get(config.baseUrl + '/');
    page = require('./main.po');
  });

  it('should include login button', function() {
    expect(page.submitIcon.getAttribute('class')).toBe('fa fa-sign-in');
  
  });
});

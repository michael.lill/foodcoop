#!/bin/bash
set -euo pipefail

while [[ "$#" -gt 0 ]]; do
    case $1 in
        -y|--year) year="$2"; shift ;;
        -f|--file) file="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

if [ -z ${year+x} ];
    then echo "-y|--year is unset";
    exit 1;
fi

if [ -z ${file+x} ];
    then echo "-f|--file no file given";
    exit 1;
fi

>&2 echo "Generating SQL-Insert for $year with template $file"
echo "INSERT INTO \"Orderables\" (name,supplier,price,year,cw) VALUES"
for calenderweek in {1..52}
do
   sed "s/YEAR,KW)/$year,$calenderweek)/" $file
done

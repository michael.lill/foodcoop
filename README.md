# Preparations

- ./start-dev-container.sh -p 9000
- cp server/config/local.env.sample.js server/config/local.env.js
- You will also need to seed the database with users.
- Or restore database from backup: docker exec -i foodcoop_postgres_1 psql -U postgres postgres < /path/to/pgdump/file

# Develop
- npm run serve

# Build
- npm run build

# Run in production

```javascript
'use strict';
process.env.PORT = 80;
process.env.NODE_ENV = 'production';
require('./server/app.js');
```

# Screenshots
![](screenshots/account_book_desktop.png)
![](screenshots/account_book_mobile.png)
![](screenshots/order_cards.png)
![](screenshots/order_list_desktop.png)
![](screenshots/order_list_mobile.png)
![](screenshots/order_matrix.png)


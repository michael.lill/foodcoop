const _ = require('lodash');
const del = require('del');
const gulp = require('gulp');
const http = require('http');
const lazypipe = require('lazypipe');
const nodemon = require('nodemon');
const runSequence = require('run-sequence');
const protractor = require('gulp-protractor').protractor;
const webdriver_update = require('gulp-protractor').webdriver_update;
const Instrumenter = require('isparta').Instrumenter;

interface IConfig {
    port: string;
}

const plugins = require('gulp-load-plugins')();
let config: IConfig;

const clientPath = 'client';
const serverPath = 'server';
const paths = {
    client: {
        assets: `${clientPath}/assets/**/*`,
        images: `${clientPath}/assets/images/**/*`,
        scripts: [
            `${clientPath}/**/!(*.spec|*.mock).ts`,
            `!${clientPath}/node_modules/**/*`,
            `!${clientPath}/{typings,test_typings}/**/*`
        ],
        styles: [`${clientPath}/{app,components}/**/*.css`],
        mainStyle: `${clientPath}/app/app.css`,
        views: `${clientPath}/{app,components}/**/*.html`,
        mainView: `${clientPath}/index.html`,
        test: [`${clientPath}/{app,components}/**/*.{spec,mock}.ts`],
        e2e: ['e2e/**/*.spec.js']
    },
    server: {
        scripts: [
            `${serverPath}/**/!(*.spec|*.integration).js`,
            `!${serverPath}/config/local.env.sample.js`
        ],
        json: [`${serverPath}/**/*.json`],
        test: {
            integration: [`${serverPath}/**/*.integration.js`, 'mocha.global.js'],
            unit: [`${serverPath}/**/*.spec.js`, 'mocha.global.js']
        }
    },
    dist: 'dist'
};

/********************
 * Helper functions
 ********************/

function onServerLog(log: any) {
    console.log(plugins.util.colors.white('[') +
        plugins.util.colors.yellow('nodemon') +
        plugins.util.colors.white('] ') +
        log.message);
}

function checkAppReady(cb: any) {
    const options = {
        host: 'localhost',
        port: config.port
    };
    http
        .get(options, () => cb(true))
        .on('error', () => cb(false));
}

// Call page until first success
function whenServerReady(cb: Function) {
    let serverReady = false;
    const appReadyInterval = setInterval(() =>
        checkAppReady((ready: boolean) => {
            if (!ready || serverReady) {
                return;
            }
            clearInterval(appReadyInterval);
            serverReady = true;
            cb();
        }),
        100);
}

function sortModulesFirst(a: any, b: any) {
    const module = /\.module\.ts$/;
    const aMod = module.test(a.path);
    const bMod = module.test(b.path);
    // inject *.module.js first
    if (aMod === bMod) {
        // either both modules or both non-modules, so just sort normally
        if (a.path < b.path) {
            return -1;
        }
        if (a.path > b.path) {
            return 1;
        }
        return 0;
    } else {
        return (aMod ? -1 : 1);
    }
}

/********************
 * Reusable pipelines
 ********************/

let lintClientScripts = lazypipe()
    .pipe(plugins.tslint, require(`./tslint.json`))
    .pipe(plugins.tslint.report);

let lintServerScripts = lazypipe()
    .pipe(plugins.eslint, `${serverPath}/.eslintrc`)
    .pipe(plugins.eslint.format);

let lintServerTestScripts = lazypipe()
    .pipe(plugins.eslint, {
        configFile: `${serverPath}/.eslintrc`,
        envs: [
            'node',
            'es6',
            'mocha'
        ]
    })
    .pipe(plugins.eslint.format);

let styles = lazypipe()
    .pipe(plugins.sourcemaps.init)
    .pipe(plugins.cleanCss, { processImportFrom: ['!fonts.googleapis.com'] })
    .pipe(plugins.autoprefixer, { browsers: ['last 1 version'] })
    .pipe(plugins.sourcemaps.write, '.');

let transpileServer = lazypipe()
    .pipe(plugins.sourcemaps.init)
    .pipe(plugins.babel, {
        "presets": ["es2015"],
        "plugins": []
    })
    .pipe(plugins.sourcemaps.write, '.');

let mocha = lazypipe()
    .pipe(plugins.mocha, {
        reporter: 'spec',
        timeout: 5000,
        require: [
            './mocha.conf'
        ]
    });

let istanbul = lazypipe()
    .pipe(plugins.istanbul.writeReports)
    .pipe(plugins.istanbulEnforcer, {
        thresholds: {
            global: {
                lines: 80,
                statements: 80,
                branches: 80,
                functions: 80
            }
        },
        coverageDirectory: './coverage',
        rootDirectory: ''
    });

/********************
 * Env
 ********************/

gulp.task('env:all', () => {
    let localConfig;
    try {
        localConfig = require(`./${serverPath}/config/local.env`);
    } catch (e) {
        localConfig = require(`./${serverPath}/config/local.env.sample`);;
    }
    plugins.env({
        vars: localConfig
    });
});
gulp.task('env:test', () => {
    plugins.env({
        vars: { NODE_ENV: 'test' }
    });
});
gulp.task('env:prod', () => {
    plugins.env({
        vars: { NODE_ENV: 'production' }
    });
});

/********************
 * Tasks
 ********************/

gulp.task('inject', (cb: Function) => {
    runSequence(['inject:js', 'inject:css'], cb);
});

gulp.task('inject:js', () => {
    return gulp.src(paths.client.mainView)
        .pipe(plugins.inject(
            gulp.src(_.union(paths.client.scripts, ['client/app/app.constant.js', `!${clientPath}/**/*.{spec,mock}.ts`, `!${clientPath}/app/app.ts`]), { read: false })
                .pipe(plugins.sort(sortModulesFirst)),
            {
                starttag: '<!-- injector:js -->',
                endtag: '<!-- endinjector -->',
                transform: (filepath: string) => '<script src="' + filepath.replace(`/${clientPath}/`, '').replace('.ts', '.js') + '"></script>'
            }))
        .pipe(gulp.dest(clientPath));
});

gulp.task('inject:css', () => {
    return gulp.src(paths.client.mainView)
        .pipe(plugins.inject(
            gulp.src(`${clientPath}/{app,components}/**/*.css`, { read: false })
                .pipe(plugins.sort()),
            {
                starttag: '<!-- injector:css -->',
                endtag: '<!-- endinjector -->',
                transform: (filepath: string) => '<link rel="stylesheet" href="' + filepath.replace(`/${clientPath}/`, '').replace('/.tmp/', '') + '">'
            }))
        .pipe(gulp.dest(clientPath));
});

gulp.task('styles', () => {
    return gulp.src(paths.client.styles)
        .pipe(styles())
        .pipe(gulp.dest('.tmp/app'));
});

gulp.task('copy:constant', ['constant'], () => {
    return gulp.src(`${clientPath}/app/app.constant.js`, { dot: true })
        .pipe(gulp.dest('.tmp/app'));
})

gulp.task('transpile:client', ['copy:constant'], () => {
    let project = require("gulp-typescript")
        .createProject("tsconfig.client.json");
    return project.src()
        .pipe(plugins.sourcemaps.init())
        .pipe(project()).js
        .pipe(plugins.sourcemaps.write('.'))
        .pipe(gulp.dest('.tmp'));
});

gulp.task('transpile:client:test', [], () => {
    return gulp.src(['client/{app,components}/**/+(*.spec|*.mock).ts'])
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.typescript()).js
        .pipe(plugins.sourcemaps.write('.'))
        .pipe(gulp.dest('.tmp/test'));
});

gulp.task('transpile:server', () => {
    return gulp.src(_.union(paths.server.scripts, paths.server.json))
        .pipe(transpileServer())
        .pipe(gulp.dest(`${paths.dist}/${serverPath}`));
});

gulp.task('lint:scripts', (cb: Function) => runSequence(['lint:scripts:client', 'lint:scripts:server'], cb));

gulp.task('lint:scripts:client', () => {
    return gulp.src(_.union(
        paths.client.scripts,
        _.map(paths.client.test, (blob: string) => '!' + blob),
        [`!${clientPath}/app/app.constant.ts`]
    ))
        .pipe(lintClientScripts());
});

gulp.task('lint:scripts:server', () => {
    return gulp.src(_.union(paths.server.scripts, _.map(paths.server.test, (blob: string) => '!' + blob)))
        .pipe(lintServerScripts());
});

gulp.task('lint:scripts:clientTest', () => {
    return gulp.src(paths.client.test)
        .pipe(lintClientScripts());
});

gulp.task('lint:scripts:serverTest', () => {
    return gulp.src(paths.server.test)
        .pipe(lintServerTestScripts());
});

gulp.task('jscs', () => {
    return gulp.src(_.union(paths.client.scripts, paths.server.scripts))
        .pipe(plugins.jscs())
        .pipe(plugins.jscs.reporter());
});

gulp.task('clean:tmp', () => del(['.tmp/**/*'], { dot: true }));

gulp.task('start:client', (cb: Function) => {
    whenServerReady(cb);
});

gulp.task('start:server', () => {
    process.env.NODE_ENV = process.env.NODE_ENV || 'development';
    config = require(`./${serverPath}/config/environment`);
    nodemon(`-w ${serverPath} ${serverPath}`)
        .on('log', onServerLog);
});

gulp.task('start:server:prod', () => {
    process.env.NODE_ENV = process.env.NODE_ENV || 'production';
    config = require(`./${paths.dist}/${serverPath}/config/environment`);
    nodemon(`-w ${paths.dist}/${serverPath} ${paths.dist}/${serverPath}`)
        .on('log', onServerLog);
});

gulp.task('start:server:debug', () => {
    process.env.NODE_ENV = process.env.NODE_ENV || 'development';
    config = require(`./${serverPath}/config/environment`);
    nodemon(`-w ${serverPath} --debug-brk ${serverPath}`)
        .on('log', onServerLog);
});

gulp.task('watch', () => {
    const testFiles = _.union(paths.client.test, paths.server.test.unit, paths.server.test.integration);


    plugins.watch(paths.client.styles, () => {  //['inject:css']
        gulp.src(paths.client.mainStyle)
            .pipe(plugins.plumber())
            .pipe(styles())
            .pipe(gulp.dest('.tmp/app'));
    });

    plugins.watch(paths.client.views)
        .pipe(plugins.plumber());

    gulp.watch(paths.client.scripts, ['lint:scripts:client', 'transpile:client']);

    plugins.watch(_.union(paths.server.scripts, testFiles))
        .pipe(plugins.plumber())
        .pipe(lintServerScripts());
});

gulp.task('serve', (cb: Function) => {
    runSequence(['clean:tmp', 'constant', 'env:all'],
        ['lint:scripts', 'inject'],
        ['transpile:client', 'styles'],
        ['start:server', 'start:client'],
        'watch',
        cb);
});

gulp.task('serve:dist', (cb: Function) => {
    runSequence(
        'build',
        'env:all',
        'env:prod',
        ['start:server:prod', 'start:client'],
        cb);
});

gulp.task('serve:debug', (cb: Function) => {
    runSequence(['clean:tmp', 'constant'],
        ['lint:scripts', 'inject'],
        ['transpile:client', 'styles'],
        ['start:server:debug', 'start:client'],
        'watch',
        cb);
});

gulp.task('test', (cb: Function) => {
    return runSequence('test:server', cb);
});

gulp.task('test:server', (cb: Function) => {
    runSequence(
        'env:all',
        'env:test',
        'mocha:unit',
        'mocha:integration',
        'mocha:coverage',
        cb);
});

gulp.task('mocha:unit', () => {
    return gulp.src(paths.server.test.unit)
        .pipe(mocha());
});

gulp.task('mocha:integration', ['env:test'], () => {
    return gulp.src(paths.server.test.integration)
        .pipe(mocha());
});

/********************
 * Build
 ********************/

gulp.task('build', (cb: Function) => {
    runSequence(
        [
            'clean:dist',
            'clean:tmp'
        ],
        'inject',
        [
            'transpile:client',
            'transpile:server'
        ],
        [
            'copy:extras',
            'copy:fonts',
            'copy:assets',
            'copy:server',
            'build:client'
        ],
        cb);
});

gulp.task('clean:dist', () => del([`${paths.dist}/!(.git*|.openshift|Procfile)**`], { dot: true }));

gulp.task('build:client', ['styles', 'html', 'constant'], () => {
    const manifest = gulp.src(`${paths.dist}/${clientPath}/assets/rev-manifest.json`);

    const appFilter = plugins.filter('**/app.js', { restore: true });
    const jsFilter = plugins.filter('**/*.js', { restore: true });
    const cssFilter = plugins.filter('**/*.css', { restore: true });
    const htmlBlock = plugins.filter(['**/*.!(html)'], { restore: true });

    return gulp.src(paths.client.mainView)
        .pipe(plugins.useref())
        .pipe(appFilter)
        .pipe(plugins.addSrc.append('.tmp/templates.js'))
        .pipe(plugins.concat('app/app.js'))
        .pipe(appFilter.restore)
        .pipe(jsFilter)
        .pipe(plugins.ngAnnotate())
        .pipe(plugins.uglify())
        .on('error', (err: any) => console.log(err))
        .pipe(jsFilter.restore)
        .pipe(cssFilter)
        .pipe(plugins.cleanCss({
            processImportFrom: ['!fonts.googleapis.com']
        }))
        .pipe(cssFilter.restore)
        .pipe(htmlBlock)
        .pipe(plugins.rev())
        .pipe(htmlBlock.restore)
        .pipe(plugins.revReplace({ manifest }))
        .pipe(gulp.dest(`${paths.dist}/${clientPath}`));
});

gulp.task('html', function () {
    return gulp.src(`${clientPath}/{app,components}/**/*.html`)
        .pipe(plugins.angularTemplatecache({
            module: 'foodcoopApp'
        }))
        .pipe(gulp.dest('.tmp'));
});

gulp.task('constant', function () {
    let sharedConfig = require(`./${serverPath}/config/environment/shared`);
    return plugins.ngConstant({
        name: 'foodcoopApp.constants',
        deps: [],
        wrap: true,
        stream: true,
        constants: { appConfig: sharedConfig }
    })
        .pipe(plugins.rename({
            basename: 'app.constant'
        }))
        .pipe(gulp.dest(`${clientPath}/app/`))
});

gulp.task('build:images', () => {
    return gulp.src(paths.client.images)
        .pipe(plugins.imagemin({
            optimizationLevel: 5,
            progressive: true,
            interlaced: true
        }))
        .pipe(plugins.rev())
        .pipe(gulp.dest(`${paths.dist}/${clientPath}/assets/images`))
        .pipe(plugins.rev.manifest(`${paths.dist}/${clientPath}/assets/rev-manifest.json`, {
            base: `${paths.dist}/${clientPath}/assets`,
            merge: true
        }))
        .pipe(gulp.dest(`${paths.dist}/${clientPath}/assets`));
});

gulp.task('copy:extras', () => {
    return gulp.src([
        `${clientPath}/favicon.ico`,
        `${clientPath}/robots.txt`,
        `${clientPath}/manifest.json`,
        `${clientPath}/.htaccess`
    ], { dot: true })
        .pipe(gulp.dest(`${paths.dist}/${clientPath}`));
});

gulp.task('copy:fonts', () => {
    return gulp.src(`node_modules/{bootstrap,font-awesome}/fonts/**/*`, { dot: true })
        .pipe(gulp.dest(`${paths.dist}/${clientPath}/node_modules`));
});

gulp.task('copy:assets', () => {
    return gulp.src([paths.client.assets, '!' + paths.client.images])
        .pipe(gulp.dest(`${paths.dist}/${clientPath}/assets`));
});

gulp.task('copy:server', () => {
    return gulp.src([
        'package.json',
        'package-lock.json',
    ], { cwdbase: true })
        .pipe(gulp.dest(paths.dist));
});

gulp.task('coverage:pre', () => {
    return gulp.src(paths.server.scripts)
        // Covering files
        .pipe(plugins.istanbul({
            instrumenter: Instrumenter, // Use the isparta instrumenter (code coverage for ES6)
            includeUntested: true
        }))
        // Force `require` to return covered files
        .pipe(plugins.istanbul.hookRequire());
});

gulp.task('coverage:unit', () => {
    return gulp.src(paths.server.test.unit)
        .pipe(mocha())
        .pipe(istanbul())
    // Creating the reports after tests ran
});

gulp.task('coverage:integration', () => {
    return gulp.src(paths.server.test.integration)
        .pipe(mocha())
        .pipe(istanbul())
    // Creating the reports after tests ran
});

gulp.task('mocha:coverage', (cb: Function) => {
    runSequence('coverage:pre',
        'env:all',
        'env:test',
        'coverage:unit',
        'coverage:integration',
        cb);
});

// Downloads the selenium webdriver
gulp.task('webdriver_update', webdriver_update);

gulp.task('test:e2e', ['env:all', 'env:test', 'start:server', 'webdriver_update'], (cb: Function) => {
    gulp.src(paths.client.e2e)
        .pipe(protractor({
            configFile: 'protractor.conf.js',
        })).on('error', (err: string) => {
            console.log(err)
        }).on('end', () => {
            process.exit();
        });
});
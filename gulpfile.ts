const _ = require('lodash');
const del = require('del');
const gulp = require('gulp');
const http = require('http');
const lazypipe = require('lazypipe');
const nodemon = require('nodemon');

interface IConfig {
    port: string;
}

const plugins = require('gulp-load-plugins')();
console.log(`Using gulp plugins: ${Object.keys(plugins).join(", ")}`);

let config: IConfig;

const clientPath = 'client';
const serverPath = 'server';
const paths = {
    client: {
        assets: `${clientPath}/assets/**/*`,
        images: `${clientPath}/assets/images/**/*`,
        scripts: [
            `${clientPath}/**/*.ts`,
        ],
        styles: [`${clientPath}/app/**/*.css`],
        mainStyle: `${clientPath}/app/app.css`,
        views: `${clientPath}/{app}/**/*.html`,
        mainView: `${clientPath}/index.html`,
        e2e: ['e2e/**/*.spec.js']
    },
    server: {
        scripts: [
            `${serverPath}/**/!(*.spec|*.integration).js`,
            `!${serverPath}/config/local.env.sample.js`
        ],
        json: [`${serverPath}/**/*.json`]
    },
    dist: 'dist'
};

/********************
 * Helper functions
 ********************/

function onServerLog(log: any) {
    console.log('[nodemon] ' + log.message);
}

function checkAppReady(cb: any) {
    const options = {
        host: 'localhost',
        port: config.port
    };
    http
        .get(options, () => cb(true))
        .on('error', () => cb(false));
}

// Call page until first success
function whenServerReady(cb: Function) {
    let serverReady = false;
    const appReadyInterval = setInterval(() =>
        checkAppReady((ready: boolean) => {
            if (!ready || serverReady) {
                return;
            }
            clearInterval(appReadyInterval);
            serverReady = true;
            cb();
        }),
        100);
}

function sortModulesFirst(a: any, b: any) {
    const module = /\.module\.ts$/;
    const aMod = module.test(a.path);
    const bMod = module.test(b.path);
    // inject *.module.js first
    if (aMod === bMod) {
        // either both modules or both non-modules, so just sort normally
        if (a.path < b.path) {
            return -1;
        }
        if (a.path > b.path) {
            return 1;
        }
        return 0;
    } else {
        return (aMod ? -1 : 1);
    }
}

/********************
 * Reusable pipelines
 ********************/

let styles = lazypipe()
    .pipe(plugins.sourcemaps.init)
    .pipe(plugins.cleanCss, { processImportFrom: ['!fonts.googleapis.com'] })
    .pipe(plugins.sourcemaps.write, '.');


/********************
 * Env
 ********************/

gulp.task('env:all', (done) => {
    let localConfig;
    try {
        localConfig = require(`./${serverPath}/config/local.env`);
    } catch (e) {
        localConfig = require(`./${serverPath}/config/local.env.sample`);;
    }
    plugins.env({
        vars: localConfig
    });
    done();
});
gulp.task('env:test', (done) => {
    plugins.env({
        vars: { NODE_ENV: 'test' }
    });
    done();
});
gulp.task('env:prod', (done) => {
    plugins.env({
        vars: { NODE_ENV: 'production' }
    });
    done();
});

/********************
 * Tasks
 ********************/

gulp.task('inject:css', () => {
    return gulp.src(paths.client.mainView)
        .pipe(plugins.inject(
            gulp.src(`${clientPath}/{app,components}/**/*.css`, { read: false })
                .pipe(plugins.sort()),
            {
                starttag: '<!-- injector:css -->',
                endtag: '<!-- endinjector -->',
                transform: (filepath: string) => '<link rel="stylesheet" href="' + filepath.replace(`/${clientPath}/`, '').replace('/.tmp/', '') + '">'
            }))
        .pipe(gulp.dest(clientPath));
});

gulp.task('inject', gulp.series('inject:css'));


gulp.task('styles', () => {
    return gulp.src(paths.client.styles)
        .pipe(styles())
        .pipe(gulp.dest('.tmp/app'));
});

gulp.task('transpile:server', () => {
    return gulp.src(_.union(paths.server.scripts, paths.server.json))
        .pipe(gulp.dest(`${paths.dist}/${serverPath}`));
});

gulp.task('clean:tmp', () => del(['.tmp/**/*'], { dot: true }));

gulp.task('start:client', (cb: Function) => {
    whenServerReady(cb);
});

gulp.task('start:server', () => {
    process.env.NODE_ENV = process.env.NODE_ENV || 'development';
    config = require(`./${serverPath}/config/environment`);
    nodemon(`--inspect=0.0.0.0:9229 -w ${serverPath} ${serverPath}`)
        .on('log', onServerLog);
});

gulp.task('start:server:prod', () => {
    process.env.NODE_ENV = process.env.NODE_ENV || 'production';
    config = require(`./${paths.dist}/${serverPath}/config/environment`);
    nodemon(`-w ${paths.dist}/${serverPath} ${paths.dist}/${serverPath}`)
        .on('log', onServerLog);
});

gulp.task('start:server:debug', () => {
    process.env.NODE_ENV = process.env.NODE_ENV || 'development';
    config = require(`./${serverPath}/config/environment`);
    nodemon(`--inspect=0.0.0.0:9229 -w ${serverPath} --debug-brk ${serverPath}`)
        .on('log', onServerLog);
});

gulp.task('watch', () => {
    gulp.watch(paths.client.styles, { interval: 1000, usePolling: true }, function (callback) {
        gulp.src(paths.client.mainStyle)
            .pipe(plugins.plumber())
            .pipe(styles())
            .pipe(gulp.dest('.tmp/app'));
        callback();
    });

    gulp.watch(paths.client.scripts, { interval: 1000, usePolling: true }, gulp.series('transpile:client'));

});

/********************
 * Build
 ********************/

gulp.task('clean:dist', () => del([`${paths.dist}/!(.git*|.openshift|Procfile)**`], { dot: true }));

gulp.task('html', function () {
    return gulp.src(`${clientPath}/{app,components}/**/*.html`)
        .pipe(plugins.angularTemplatecache({
            module: 'foodcoopApp'
        }))
        .pipe(gulp.dest('.tmp'));
});

gulp.task('transpile:client', (callback) => {
    require('esbuild').build({
        entryPoints: [`${clientPath}/app/app.ts`],
        outfile: './.tmp/app/app.js',
        minify: true,
        bundle: true,
        sourcemap: true,
        target: 'es2016'
    }).catch(() => process.exit(1));
    callback();
});

gulp.task('build:client', gulp.series('styles', 'html', () => {
    const manifest = gulp.src(`${paths.dist}/${clientPath}/assets/rev-manifest.json`, { allowEmpty: true });

    const appFilter = plugins.filter('**/app.js', { restore: true });
    const jsFilter = plugins.filter('**/*.js', { restore: true });
    const cssFilter = plugins.filter('**/*.css', { restore: true });
    const htmlBlock = plugins.filter(['**/*.!(html)'], { restore: true });

    return gulp.src(paths.client.mainView)
        .pipe(plugins.useref())
        .pipe(appFilter)
        .pipe(plugins.addSrc.append('.tmp/templates.js'))
        .pipe(plugins.concat('app/app.js'))
        .pipe(appFilter.restore)
        .pipe(cssFilter)
        .pipe(plugins.cleanCss({
            processImportFrom: ['!fonts.googleapis.com']
        }))
        .pipe(cssFilter.restore)
        .pipe(htmlBlock)
        .pipe(plugins.rev())
        .pipe(htmlBlock.restore)
        .pipe(plugins.revReplace({ manifest }))
        .pipe(gulp.dest(`${paths.dist}/${clientPath}`));
}));

gulp.task('copy:extras', () => {
    return gulp.src([
        `${clientPath}/favicon.ico`,
        `${clientPath}/manifest.json`
    ], { dot: true })
        .pipe(gulp.dest(`${paths.dist}/${clientPath}`));
});

gulp.task('copy:fonts', (callback) => {
    require("fs").symlink(
        `../node_modules`
        , `${paths.dist}/${clientPath}/node_modules`
        , 'dir'
        , function (err) { callback(); }
    );
});

gulp.task('copy:assets', () => {
    return gulp.src([paths.client.assets])
        .pipe(gulp.dest(`${paths.dist}/${clientPath}/assets`));
});

gulp.task('copy:server', () => {
    return gulp.src([
        'package.json',
        'package-lock.json',
    ], { cwdbase: true })
        .pipe(gulp.dest(paths.dist));
});

gulp.task('build', gulp.series(
    'clean:dist',
    'clean:tmp',
    'inject',
    'transpile:client',
    'transpile:server',
    'copy:extras',
    'copy:fonts',
    'copy:assets',
    'copy:server',
    'build:client'
));

gulp.task('serve', gulp.series('clean:tmp', 'env:all',
    'inject',
    'transpile:client', 'styles',
    'start:server', 'start:client',
    'watch'));

gulp.task('serve:dist', gulp.series(
    'build',
    'env:all',
    'env:prod',
    'start:server:prod',
    'start:client'));

gulp.task('serve:debug', gulp.series(
    'clean:tmp', 'inject',
    'transpile:client', 'styles',
    'start:server:debug', 'start:client',
    'watch'));

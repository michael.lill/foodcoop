import app from './';

before((done) => {
  app._ready.then(done);
});

after(function (done) {
  app.angularFullstack.on('close', () => done());
  app.angularFullstack.close();
});

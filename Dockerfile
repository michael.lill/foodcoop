FROM node:alpine

RUN npm cache clean -f; npm install -g npm

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

ENV NODE_PATH=/usr/local/lib/node_modules/:/usr/local/lib  NODE_ENV=production

COPY dist/ /usr/src/app/

RUN apk add --update python make gcc g++
RUN npm install 

ONBUILD RUN npm install 

CMD [ "npm", "start" ]

EXPOSE 8080

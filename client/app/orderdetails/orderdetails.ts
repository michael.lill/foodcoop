'use strict';

angular.module('foodcoopApp').config(function($routeProvider) {
  $routeProvider.when('/orderdetails/:year/:cw',
                      {template : '<orderdetails></orderdetails>'});
});

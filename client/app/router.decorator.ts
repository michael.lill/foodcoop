import {noop} from "lodash";

export function routerDecorator($rootScope, $location, Auth, $routeParams) {
  // Redirect to login if route requires auth and the user is not logged
  // in, or doesn't have required role
  $rootScope.$on('$routeChangeStart', function (event, next, current) {
    if (next.name === 'logout' && current && current.originalPath &&
      !current.authenticate) {
      next.referrer = current.originalPath;
      return;
    }

    if (typeof next.authenticate === 'string') {
      Auth.hasRole(next.authenticate, noop).then(has => {
        if (has) {
          return;
        }

        event.preventDefault();
        return Auth.isLoggedIn(noop).then(
          is => { $location.path(is ? '/' : '/login'); });
      });
    } else {
      Auth.isLoggedIn(noop).then(is => {
        if (is) {
          return;
        }
        event.preventDefault();
        $location.path('/');
      });
    }
  });
}

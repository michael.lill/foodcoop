
import {KW} from '../shared/kw';

export class Service {
    private currentKW: KW;

    public thisWeek = () => new KW(parseInt(this.moment().format('GGGG')),
        parseInt(this.moment().format('WW')));

    constructor(
        private $http: angular.IHttpService, private $resource, private $route,
        private $routeParams, private moment) {
        // Momentane Woche wählen
        this.currentKW = this.thisWeek();
        // Oder überschreiben mit den Routeparams, falls diese existieren
        if (this.$routeParams.year) {
            this.currentKW.year = parseInt(this.$routeParams.year);
            this.currentKW.cw = parseInt(this.$routeParams.cw);
        }
    }

    public getSelectedCw = (): KW => { return this.currentKW; };

    private isPastWednesdayEvening =
        () => {
            return this.moment().weekday() > 2 ||
                (this.moment().weekday() === 2 && this.moment().hour() >= 21);
        }

    public IsClosedForOrdersAsync =
        () => {
            return this.getSetting('lockOrders')
                .then(response => response.data && response.data[0].value)
                .then(kwJson => JSON.parse(kwJson) as KW)
                .then(lockedKW => lockedKW.year > this.currentKW.year
                    || (lockedKW.year === this.currentKW.year
                        && lockedKW.cw >= this.currentKW.cw) );
        }

    public orderables = () => {
        return this.$resource('/api/orderables/:id', { id: '@_id' }, {
            query: {
                method: 'GET',
                isArray: true,
                url: '/api/orderables/' + this.currentKW.year + '/' + this.currentKW.cw
            },
            update: { method: 'PUT' }
        });
    };

    public recentProducts = () => {
        return this.$resource('/api/recentProducts', {  }, {
            query: {
                method: 'GET',
                isArray: true,
                url: '/api/recentProducts/'
            }
        });
    };

    public sendNotifications = (year: number = this.currentKW.year, cw: number = this.currentKW.cw) =>
        this.$http.get(`/api/sendnotifications/${year}/${cw}`);

    public testMail = () => this.$http.get(`/api/testmail`);

    public changeMail = (email: string) => this.$http.post('/api/users/changeMail', {
        email: email
    });

    public inventory =
        (startyear: number, startcw: number, endyear: number, endcw: number) => {
            return this.$http({
                method: 'GET',
                url: `/api/inventory/${startyear}/${startcw}/${endyear}/${endcw}`
            });
        };

    public allMyProducts = () => {
        return this.$http({
            method: 'GET',
            url: `/api/allMyProducts`
        });
    };

    public orderDetails = (year = this.currentKW.year, week = this.currentKW.cw) => {
        return this.$http({
            method: 'GET',
            url: '/api/orderDetails/' + year + '/' + week
        });
    };

    public products = (year = this.currentKW.year, week = this.currentKW.cw) => {
        return this.$resource('/api/products/:id', { id: '@_id' }, {
            query: {
                method: 'GET',
                isArray: true,
                url: '/api/products/' + year + '/' + week
            },
            update: { method: 'PUT' }
        });
    };

    public nonperishables = () => {
        return this.$resource('/api/nonperishable/:id', { id: '@_id' }, {
            query: {
                method: 'GET',
                isArray: true,
                url: '/api/nonperishable'
            },
            update: { method: 'PUT' }
        });
    };

    public getSetting = (key) => {
        return this.$http({
            method: 'GET',
            url: `/api/settings/${key}`
        });
    };

    public upsertSetting = (entity) => {
        if (!entity._id) {
            return this.$http({
                method: 'POST',
                url: `/api/settings`,
                data: entity
            });
        } else {
            return this.$http({
                method: 'PUT',
                url: `/api/settings/${entity._id}`,
                data: entity
            });
        }
    };

    public addProduct =
        (name = '', quantity = 1, pricePerQuantity = 0, OrderableId = undefined) => {
            return this.products().save({
                cw: this.currentKW.cw,
                year: this.currentKW.year,
                name: name,
                quantity: quantity,
                pricePerQuantity: pricePerQuantity,
                OrderableId: OrderableId
            });
        }

    public updateProduct =
        (product, name, supplier, price, orderableId, quantity = 0) => {
            product.name = name + '♦' + supplier;
            product.OrderableId = orderableId;
            product.quantity = quantity;
            product.pricePerQuantity = price;
            return product.$update();
        };

    public transactions = () => { return this.$resource('/api/transactions'); };

    public balance =
        () => { return this.$http({ method: 'GET', url: '/api/balance' }); };

    public getStartBalance = () => {
        return this.$http({
            method: 'GET',
            url: '/api/startBalance/' + this.currentKW.year + '/' + this.currentKW.cw
        });
    };

    public getBalances = (year: number, cw: number) => {
        return this.$http({
            method: 'GET',
            url: '/api/balances/' + year + '/' + cw
        });
    };

    private updateRoute(kw: KW) {
        this.$route.updateParams({
            year: kw.year,
            cw: kw.cw
        });
    }

    public prevWeek = () => {
        this.currentKW = this.currentKW.prev();
        this.updateRoute(this.currentKW);
    };

    public nextWeek = () => {
        this.currentKW = this.currentKW.next();
        this.updateRoute(this.currentKW);
    };

    public setWeek = (year: number, cw: number) => {
        this.currentKW.year = year;
        this.currentKW.cw = cw;
        this.updateRoute(this.currentKW);
    }

    public monday = () => {
        return this.moment(
            this.currentKW.year.toString() + this.currentKW.cw.toString(),
            'GGGGWW');
    };

    public getTurnover = () => {
        return this.$http({
            method: 'GET',
            url: `/api/turnover`
        });
    };

    public getUsers = () => {
        return this.$http({
            method: 'GET',
            url: '/api/users'
        });
    };

    public setPassword(userId, password){
        return this.$http({
            method: 'POST',
            url: '/api/users/setPassword',
            data: {
                userId: userId,
                password: password
            }
        })
    }
}

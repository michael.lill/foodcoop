

export function authInterceptor($rootScope, $q, $cookies, $location, Util) {
  return {
    // Add authorization token to headers
    request(config) {
      config.headers = config.headers || {};
      if ($cookies.get('token') && Util.isSameOrigin(config.url)) {
        config.headers.Authorization = 'Bearer ' + $cookies.get('token');
      }
      return config;
    },

    responseError(response) {
      on4xxResponseRedirectToLogin(response, $location, $cookies);
      return $q.reject(response);
    }
  };
}

function on4xxResponseRedirectToLogin(response: any, $location: any, $cookies: any) {
  if (response.status >= 400 && response.status < 500) {
    $location.path('/login');
    // remove any stale tokens
    $cookies.remove('token', {
      secure: false,
      samesite: 'strict'
    });
  }
}

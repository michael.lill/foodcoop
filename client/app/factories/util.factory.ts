import * as toastr from 'toastr';

export function httpInjector($q: angular.IQService) {
    let errorHandler = function (rejection) {
        if (rejection.status === 403 && typeof (rejection.data) === typeof ('')) {
            toastr.error(rejection.data);
        } else if (rejection.status !== 401) {
            toastr.error('Fehler beim Speichern / Abrufen von Daten.');
        }
        return $q.reject(rejection);
    };
    let passThrough = function (input) { return input; };
    return {
        'request': passThrough,
        'requestError': errorHandler,
        'response': passThrough,
        'responseError': errorHandler
    };
}

export function focus($timeout, $window) {
    return (id) => {
        // timeout makes sure that it is invoked after any other event has
        // been triggered.
        // e.g. click events that need to run before the focus or
        // inputs elements that are in a disabled state but are enabled when
        // those events
        // are triggered.
        $timeout(() => {
            let element = $window.document.getElementById(id);
            if (element) {
                element.focus();
            }
        });
    };
}


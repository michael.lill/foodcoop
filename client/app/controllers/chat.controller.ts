import { Json } from "sequelize/types/lib/utils";
import { Service } from "../services/service.service"

const reconnectTimeout = 1000;
export class ChatController {
    private messageText = "";
    private messages = [];
    private isLoggedIn;
    private visible = false;
    private socket: WebSocket;
    constructor(private service: Service, private Auth, private $scope) {
        this.isLoggedIn = Auth.isLoggedIn;
        this.connectWebSocket();
        $scope.$on('$routeChangeStart', () => {
            this.connectWebSocket();
        });
    }

    private connectWebSocket() {
        if(!this.Auth){
            return;
        }
        this.Auth.isLoggedIn(isLoggedin => {
            if (!isLoggedin) {
                return;
            }

            if (this.socket && this.socket.readyState !== this.socket.CLOSED) {
                return;
            }

            this.socket = new WebSocket(location.origin.replace(/^http/, 'ws'));

            this.socket.onopen = () => this.socket.send('loadMessages');

            this.socket.onclose = (event) => {
                setTimeout(this.connectWebSocket, reconnectTimeout);
            };

            this.socket.onerror = (error) => {
                this.socket.close();
            };

            this.socket.onmessage = (event => {
                this.messages = this.messages.concat(JSON.parse(event.data));
                this.markRead();
                this.$scope.$apply();
                const objDiv = document.getElementById("foodcoop-messages");
                if (objDiv) {
                    objDiv.scrollTop = objDiv.scrollHeight;
                }
            });
        });
    }

    hasNewMessages = () => {
        if (this.messages.length === 0) {
            return false;
        }
        const lastMessage = this.messages[this.messages.length - 1];
        return window.localStorage.getItem('dateTimeOfLastMessage') !== lastMessage.createdAt;
    }

    markRead = () => {
        if (this.messages.length === 0 || !this.visible) {
            return false;
        }
        const lastMessage = this.messages[this.messages.length - 1];
        window.localStorage.setItem('dateTimeOfLastMessage', lastMessage.createdAt);
    }

    send = () => {
        if (this.messageText.length === 0) {
            return;
        }
        this.socket.send(JSON.stringify({
            userId: this.Auth.getCurrentUser()._id,
            text: this.messageText
        }));
        this.messageText = "";
    }
}
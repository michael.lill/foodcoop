

export class ModalController {
  constructor(
    private $uibModalInstance, private $scope, private $uibModal,
    private hotkeys) {
    $scope.ok = this.ok;
    $scope.cancel = this.cancel;
    hotkeys.bindTo($scope).add(
      {
        combo: 'enter', description: 'ok', callback: (ev: KeyboardEvent) => {
          this.ok();
          ev.preventDefault();
        }
      }
    );
  }
  private ok = () => this.$uibModalInstance.close(this.$scope.answer);
  private cancel = () => this.$uibModalInstance.dismiss('cancel');
}

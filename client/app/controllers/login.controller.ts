

export class LoginController {
  private user;
  private errors;
  private submitted: Boolean;
  constructor(private Auth, private $location, private service: Service,
              private $route) {
    this.user = {};
    this.errors = {};
    this.submitted = false;
    Auth.logout();
  }

  public login = (form) => {
    this.submitted = true;
    if (form.$valid) {
      this.Auth.login({name : this.user.name, password : this.user.password})
          .then(() => {
            this.$location.path('/orderbook/' +
                                this.service.getSelectedCw().year + '/' +
                                this.service.getSelectedCw().cw);
          })
          .catch(err => { this.errors.other = err.message; });
    }
  }
}

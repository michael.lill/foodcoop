import { KW } from '../shared/kw';

export class NavbarController {
  public menu = [{ 'title': 'Home', 'link': '/login' }];

  public isCollapsed = true;

  private getSelectedCw: KW;
  private isLoggedIn: Function;
  private isAdmin: boolean;
  private getCurrentUser: any;

  constructor(private $location, Auth, service, $scope) {
    this.getSelectedCw = service.getSelectedCw;
    this.isLoggedIn = Auth.isLoggedIn;
    this.isAdmin = Auth.isAdmin;
    this.getCurrentUser = Auth.getCurrentUser;
    $scope.isNavCollapsed = true;
  }

  public isActive(route) { return route === this.$location.path(); }
}


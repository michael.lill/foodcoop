export function routing($routeProvider) {
    $routeProvider
        .when('/admin/:year/:cw', { template: '<admin></admin>' })
        .when('/inventory/:startyear/:startcw/:endyear/:endcw',
            { template: '<inventory></inventory>' })
        .when('/orderables/:year/:cw',
            { template: '<orderables></orderables>' })
        .when('/orderbook/:year/:cw',
            { template: '<orderbook></orderbook>' })
        .when('/orderdetails/:year/:cw',
            { template: '<orderdetails></orderdetails>' })
        .when('/orderlists/:year/:cw',
            { template: '<orderlists></orderlists>' })
        .when('/ordersummary/:year/:cw',
            { template: '<ordersummary></ordersummary>' })
        .when('/transactions/:year/:cw',
            { template: '<transactions></transactions>' })
        .when('/faq',
            { template: '<faq></faq>' })
        .when('/login', {
            templateUrl: '/app/templates/login.html',
            controller: 'LoginController',
            controllerAs: 'vm'
        })
        .when('/', {
            templateUrl: '/app/templates/login.html',
            controller: 'LoginController',
            controllerAs: 'vm'
        })
        .when('/logout', {
            name: 'logout',
            referrer: '/',
            template: '',
            controller: function ($location, $route, Auth) {
                let referrer = $route.current.params.referrer ||
                    $route.current.referrer || '/';
                Auth.logout();
                $location.path(referrer);
            }
        })
        .when('/turnover',
            { template: '<turnover></turnover>' })
        .when('/nonperishables',
            { template: '<nonperishables></nonperishables>' });
};

angular
  .module(
  'foodcoopApp',
  [
    'foodcoopApp.auth', 'foodcoopApp.constants', 'ngCookies',
    'ngResource', 'ngSanitize', 'ngRoute', 'ui.bootstrap',
    'angular-loading-bar', 'angularMoment', 'ngPatternRestrict',
    'cfp.hotkeys'
  ])
  .run(function (amMoment) { amMoment.changeLocale('de'); })
  .config(($routeProvider, $locationProvider: angular.ILocationProvider,
    cfpLoadingBarProvider, $httpProvider: angular.IHttpProvider) => {
    $locationProvider.html5Mode(true);
    cfpLoadingBarProvider.latencyThreshold = 500;
    $httpProvider.interceptors.push('httpInjector');
    // OpenShift-Hacks
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
  })
  .constant(
  'consts',
  { suppliers: ['Petrik', 'Blanc', 'Wüst', 'Fasanen', 'Schleinkofer', 'Trockenwaren'], rent: 5 });

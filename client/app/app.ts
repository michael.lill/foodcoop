import groupBy from 'lodash/groupBy';
import map from 'lodash/map';
import union from 'lodash/union';
import noop from 'lodash/noop';
import uniq from 'lodash/uniq';
import reduce from 'lodash/reduce';
import sortBy from 'lodash/sortBy';
import filter from 'lodash/filter';
import keys from 'lodash/keys';
import find from 'lodash/find';

import * as toastr from 'toastr';

import * as angular from 'angular';

import 'angular-resource';
import 'angular-cookies';
import 'angular-sanitize';
import 'angular-route';
import 'angular-ui-bootstrap';
import 'angular-loading-bar';
import 'angular-hotkeys';
import 'ng-pattern-restrict';
import 'moment';
import 'moment/locale/de';
import 'angular-moment';

import './i18n/angular-locale_de';

import { AdminComponent } from './components/admin.controller';
import { authInterceptor } from './services/interceptor.service';
import { AuthService } from './services/auth.service';
import { confirmDelete, germanNumber, keybindings } from './directives/util.directive';
import { cwSelector } from './directives/cwSelector.directive';
import { FaqComponent } from './components/faq.controller';
import { httpInjector, focus } from './factories/util.factory';
import { InventoryComponent } from './components/inventory.controller';
import { LoginController } from './controllers/login.controller';
import { ModalController } from './controllers/modal.controller';
import { NavbarController } from './controllers/navbar.controller';
import { ODSclass } from './shared/ods_writer';
import { OrderablesComponent } from './components/orderables.controller';
import { OrderbookComponent } from './components/orderbook.controller';
import { OrderdetailsComponent } from './components/orderdetails.controller';
import { OrderlistsComponent } from './components/orderlists.controller';
import { OrdersummaryComponent } from './components/ordersummary.controller';
import { routerDecorator } from './router.decorator';
import { routing } from './routing';
import { Service } from './services/service.service';
import { TransactionsComponent } from './components/transactions.controller';
import { TurnoverComponent } from './components/turnover.controller';
import { NonperishablesComponent } from './components/nonperishables.controller';
import { UserResource } from './services/user.service';
import { UtilService } from './services/util.service';
import { ChatController } from './controllers/chat.controller';

window._ = {
  groupBy,
  map,
  union,
  noop,
  uniq,
  reduce,
  sortBy,
  filter,
  find,
  keys
};
window.toastr = toastr;

angular
  .module(
    'foodcoopApp',
    ['ngCookies',
      'ngResource', 'ngSanitize', 'ngRoute', 'ui.bootstrap',
      'angular-loading-bar', 'angularMoment', 'ngPatternRestrict',
      'cfp.hotkeys'
    ])
  .run(['amMoment', function (amMoment) { amMoment.changeLocale('de'); }])
  .run(['$rootScope', '$location', 'Auth', '$routeParams', routerDecorator])
  .config(['$locationProvider', 'cfpLoadingBarProvider', '$httpProvider',
    ($locationProvider: angular.ILocationProvider, cfpLoadingBarProvider, $httpProvider: angular.IHttpProvider) => {
      $locationProvider.html5Mode(true);
      cfpLoadingBarProvider.latencyThreshold = 500;
      $httpProvider.interceptors.push('httpInjector');
      $httpProvider.interceptors.push('authInterceptor');
    }])
  .config(['$routeProvider', routing])
  .constant(
    'consts',
    { rent: 5 })
  .component('admin', {
    templateUrl: '/app/templates/admin.html',
    controller: ['$scope', '$location', 'service', 'Auth', AdminComponent]
  }).component('faq', {
    templateUrl: '/app/templates/faq.html',
    controller: ['Auth', 'service', 'odsWriter', FaqComponent]
  }).component('inventory', {
    templateUrl: '/app/templates/inventory.html',
    controller: ['$scope', 'service', 'moment', '$routeParams',
      '$route', 'odsWriter', InventoryComponent]
  }).component('orderables', {
    templateUrl: '/app/templates/orderables.html',
    controller: ['$scope', 'service', '$uibModal', 'focus', 'Auth', '$cookies', '$window', OrderablesComponent]
  }).component('orderbook', {
    templateUrl: '/app/templates/orderbook.html',
    controller: ['$scope', 'service', 'moment', 'consts', '$window', '$timeout', 'hotkeys', 'Auth', '$cookies', OrderbookComponent]
  }).component('orderdetails', {
    templateUrl: '/app/templates/orderdetails.html',
    controller: ['$scope', 'service', OrderdetailsComponent]
  }).component('orderlists', {
    templateUrl: '/app/templates/orderlists.html',
    controller: ['$scope', 'consts', 'service', '$window', '$cookies', OrderlistsComponent]
  }).component('ordersummary', {
    templateUrl: '/app/templates/ordersummary.html',
    controller: ['$scope', 'service', OrdersummaryComponent]
  }).component('transactions', {
    templateUrl: '/app/templates/transactions.html',
    controller: ['$scope', 'service', 'moment', TransactionsComponent]
  }).component('turnover', {
    templateUrl: '/app/templates/turnover.html',
    controller: ['$scope', 'service', 'moment', TurnoverComponent]
  }).component('nonperishables', {
    templateUrl: '/app/templates/nonperishables.html',
    controller: ['$scope', 'service', 'moment', NonperishablesComponent]
  })
  .controller('NavbarController', ['$location', 'Auth', 'service', '$scope', NavbarController])
  .controller('LoginController', ['Auth', '$location', 'service', '$route', LoginController])
  .controller('ModalController', ['$uibModalInstance', '$scope', '$uibModal', 'hotkeys', ModalController])
  .service('service', ['$http', '$resource', '$route', '$routeParams', 'moment', Service])
  .service('odsWriter', [ODSclass])
  .directive('navbar', () => ({
    templateUrl: '/app/templates/navbar.html',
    restrict: 'E',
    controller: ['$location', 'Auth', 'service', '$scope', NavbarController],
    controllerAs: 'nav'
  }))
  .directive('cwSelector', ['service', cwSelector])
  .factory('httpInjector', ['$q', httpInjector])
  .factory('focus', ['$timeout', '$window', focus])
  .directive('ngConfirmDelete', [confirmDelete])
  .directive('germanNumber', [germanNumber])
  .directive('keybindings', [keybindings])
  .factory('User', ['$resource', UserResource])
  .factory('Auth', ['$location', '$http', '$cookies', '$q', 'appConfig', 'Util', 'User', '$window', AuthService])
  .factory('authInterceptor', ['$rootScope', '$q', '$cookies', '$location', 'Util', authInterceptor])
  .factory('Util', ['$window', UtilService])
  .directive('chat', () => ({
    templateUrl: '/app/templates/chat.html',
    restrict: 'E',
    controller: ['service', 'Auth', '$scope', ChatController],
    controllerAs: 'chat'
  }))
  .constant("appConfig", { "userRoles": ["guest", "user", "admin"] });



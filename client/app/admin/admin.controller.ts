'use strict';
(function () {

  class AdminComponent {
    constructor($scope, $location: angular.ILocationService, service: Service) {
      $scope.getSelectedCw = service.getSelectedCw;
      $scope.$location = $location;
      $scope.isActive = function (route) {
        return route === $scope.$location.path();
      };
      $scope.sql = 
`select * from orderables limit 10;
select * from products limit 10;
select * from users limit 10;
select * from settings;`;
      $scope.query = () => {
        service.sql($scope.password, $scope.sql).then((result) => {
          $scope.sqlresult = JSON.stringify(result.data);
        }).catch(reason => {
          $scope.sqlresult = JSON.stringify(reason);
        })
      };
      $scope.setLock = () => service.getSetting('lockOrders').then((response) => {
         let template = ((response.data && response.data[0]) || {}) as any;
         template.key = 'lockOrders';
         template.value = JSON.stringify(service.getSelectedCw());
         service.upsertSetting(template)
          .then(() => toastr.success(`Bestellungen beendet ab KW ${service.getSelectedCw().cw}`))
      });
    }
  }

  angular.module('foodcoopApp').component('admin', {
    templateUrl: 'app/admin/admin.html',
    controller: AdminComponent
  });

})();

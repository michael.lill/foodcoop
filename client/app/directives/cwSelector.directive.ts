

import { Service } from '../services/service.service';
import { KW } from '../shared/kw';

export function cwSelector(service: Service) {
  return {
    templateUrl: '/app/templates/cwSelector.html',
    restrict: 'EA',
    link: function (scope: any, element, attrs) {
      scope.getSelectedCw = service.getSelectedCw;
      scope.monday = service.monday;
      scope.nextWeek = function () { service.nextWeek(); };
      scope.prevWeek = function () { service.prevWeek(); };
      scope.setWeek = service.setWeek;
      scope.thisWeek = service.thisWeek();
      scope.chooserVisible = false;
      const weeks = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
        11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
        21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
        31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
        41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
        51, 52];
      const thisYear = (new Date()).getFullYear();
      const allWeeks = weeks
        .filter(week => week > 25)
        .map(week => new KW(thisYear - 1, week))
        .concat(weeks.filter(week => week < scope.thisWeek.cw + 3)
          .map(week => new KW(thisYear, week)))
      const numberOfShowWeeks = 30;
      scope.cws = allWeeks.slice(allWeeks.length - numberOfShowWeeks);
    }
  };
}

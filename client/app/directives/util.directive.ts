

export function confirmDelete() {
    return {
        link: function (
            scope: angular.IScope,
            element: angular.IRootElementService, attr: any) {
            let clickAction = attr.confirmedDelete;
            element.bind('click', function (event) {
                if (window.confirm(
                    attr.ngConfirmDelete + ' wirklich löschen?')) {
                    scope.$eval(clickAction);
                }
            });
        }
    };
}

export function germanNumber() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: (scope: angular.IScope, element: angular.IRootElementService,
            attr, ngModel: angular.INgModelController) => {

            ngModel.$parsers.push((value) => {
                if (typeof value === typeof '') {
                    if (value === '-') {
                        return parseFloat('-0');
                    }
                    if (value !== '') {
                        return parseFloat(value.replace(',', '.'));
                    }
                }
                return 0;
            });

            ngModel.$formatters.push((value: number) => {
                if (value) {
                    return value.toString().replace('.', ',');
                }
                return '0';
            });

        }
    };
}

export function keybindings() {
    return {
        restrict: 'A', // only activate on element attribute
        require: '?ngModel', // get a hold of NgModelController
        link: function (scope, element, attrs, ngModel: any) {
            if (!ngModel) {
                throw 'Kein Model'; // do nothing if no ng-model
            }

            element.on('focus', () => {
                (element[0] as any).select();
            });

            element.on('keypress', (ev) => {
                // Let cfp-hotkeys get a hold of our keypress
                let handeledByHotkeys = ['?', '+', 'Enter'];
                if (handeledByHotkeys.indexOf(ev.key) >= 0) {
                    (ev.target as any).className += ' mousetrap';
                    ev.preventDefault();
                }

                let tabindex = parseInt(element.attr('tabindex'));
                if (!tabindex) {
                    return;
                }

                if (ev.key === 'ArrowRight') {
                    tabindex += 1;
                } else if (ev.key === 'ArrowLeft') {
                    tabindex -= 1;
                } else if (ev.key === 'ArrowUp') {
                    tabindex -= 3;
                } else if (ev.key === 'ArrowDown') {
                    tabindex += 3;
                } else {
                    return;
                }
                let el = document.querySelector(`[tabindex="${tabindex}"]`) as any;

                if (el) {
                    el.select();
                }
            });

        }
    };
}

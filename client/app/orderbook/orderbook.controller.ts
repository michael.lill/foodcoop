class OrderbookComponent {
    private deposit: any;
    private thereShouldBeRent = () => {
        return this.$scope.isFirstWeekInMonth && !this.$scope.rent.quantity &&
            this.$window.confirm(
                'Es wurde bisher keine Miete gebucht. Miete von ' +
                this.consts.rent + '€ buchen?');
    };
    private askForRent =
        () => {
            const year = this.service.getSelectedCw().year;
            const cw = this.service.getSelectedCw().cw;
            if (this.thereShouldBeRent()) {
                // tslint:disable-next-line:new-parens
                this.$scope.rent = new (this.service.products());
                this.$scope.rent.name = 'rent';
                this.$scope.rent.quantity = 1;
                this.$scope.rent.price = this.consts.rent;
                this.$scope.rent.pricePerQuantity = this.consts.rent;
                this.$scope.rent.cw = cw;
                this.$scope.rent.year = year;
                this.$scope.rent.$save();
                toastr.success(
                    'Es wurde die Standard-Miete für diesen Monat gebucht. Bitte anpassen, falls Bedarf.');
            }
        }

    private tryFindSimilar(product: any, orderables: Array<any>) {
        for (let orderable of orderables) {
            let productName = (product.name as string).toLowerCase();
            let orderableName = (orderable.name + '♦' + orderable.supplier).toLowerCase();
            if (levenshtein(productName, orderableName) <= 1) {
                return {
                    old: product,
                    'new': orderable
                };
            }
        }
        return {
            old: product,
            'new': undefined
        };
    }

    constructor(
        private $scope, private service: Service, private moment, private consts,
        private $window, $timeout, private hotkeys, private Auth) {
        $scope.rent = {};
        $scope.suppliers = _.filter(consts.suppliers, supplier => supplier !== 'Trockenwaren');

        service.IsClosedForOrdersAsync().then(data => {
            $scope.IsClosedForOrders = data;
        })
        
        let ggggww = service.getSelectedCw().year.toString() +
            service.getSelectedCw().cw.toString();
        $scope.isFirstWeekInMonth =
            moment(ggggww, ['GGGGWW']).add(1, 'days').format('D') <= 7;

        $scope.updateDeposit = (entity) => {
            if (typeof this.deposit !== 'object') {
                // tslint:disable-next-line:new-parens
                this.deposit = new (service.products());
                this.deposit.name = 'deposit';
                this.deposit.quantity = 1;
                this.deposit.pricePerQuantity = -$scope.deposit;
                this.deposit.price = -$scope.deposit;
                this.deposit.cw = service.getSelectedCw().cw;
                this.deposit.year = service.getSelectedCw().year;
                this.deposit.$save().then(
                    () => { $scope.myForm['deposit'].$setPristine(); });
            } else {
                this.deposit.pricePerQuantity = -$scope.deposit;
                this.deposit.price = -$scope.deposit;
                this.deposit.$update().then(
                    () => { $scope.myForm['deposit'].$setPristine(); });
            }
        };

        $scope.toggleWednesday = () => {
            if (this.$scope.collectsOnWednesday) {
                this.$scope.collectsOnWednesday.$remove();
                this.$scope.collectsOnWednesday = undefined;
            } else {
                this.$scope.collectsOnWednesday = this.service.addProduct('wednesday');
            }
        };

        $scope.selectOrderable = ($item, $model, $label, product) => {
            this.service.updateProduct(
                product, $item.name, $item.supplier, $item.price, $item._id, 1);
        };

        $scope.selectProduct = ($item, $model, $label, product) => {
            product.pricePerQuantity = $item.pricePerQuantity;
            product.name = $item.name;
            product.$update();
        };

        $scope.sumProducts = function () {
            return _.reduce($scope.products, (total, n: any) => {
                if (n.name === 'deposit' || n.name === 'rent') {
                    return total;
                }
                return total + n.price;
            }, 0);
        };

        $scope.delete = (product, index) => {
            product.$remove().then(() => $scope.products.splice(index, 1));
        };

        $scope.update = function (product, key) {
            product.price = product.quantity * product.pricePerQuantity;
            return product.$update().then(
                () => { $scope.myForm[key].$setPristine(); });
        };

        $scope.calcEndBalance = function () {
            if (!$scope.products) {
                return $scope.startBalance;
            }
            let purchases = _.reduce($scope.products, function (total, n: any) {
                return total + n.price;
            }, 0);
            return $scope.startBalance - purchases
                - ($scope.rent.price ? $scope.rent.price : 0)
                - ($scope.deposit ? -$scope.deposit : 0);
        };

        service.getStartBalance().then(function (result) {
            $scope.startBalance = result.data;
        });

        $scope.tryOrderSameAsLastWeek = () => {
            let previousWeek = this.service.getSelectedCw().prev();
            this.service.orderDetails(previousWeek.year, previousWeek.cw).then(result => {
                let matchResult = (result.data as Array<any>)
                    .filter(value => value.UserId === this.Auth.getCurrentUser()._id)
                    .filter(value => !!value.OrderableId)
                    .map(element => this.tryFindSimilar(element, $scope.orderables));
                matchResult.filter(x => !!x.new)
                    .forEach(x => $scope.products.push(this.service.addProduct(x.new.name + '♦' + x.new.supplier, x.old.quantity, x.new.price, x.new._id)));
                let notFound = matchResult.filter(x => !x.new).map(x => x.old.name);
                if (notFound.length > 0) {
                    toastr.error(`Diese Produkte konnten nicht gefunden werden: ${notFound.join(', ')}`);
                }
            });
        };

        $scope.orderables = service.orderables().query((result) => {
            $scope.missingSupplier = (name: String) => {
                return !_.find(result, (p: any) => {
                    return p.supplier && p.supplier.indexOf(name) >= 0;
                });
            };
        });

        $scope.recentProducts = service.recentProducts().query();

        service.products().query((res) => {
            for (let i = 0; i < res.length; i++) {
                if (res[i].name === 'deposit') {
                    this.deposit = res[i];
                    this.$scope.deposit = -this.deposit.price;
                    // this maybe that todo
                    break;
                }
            }
            for (let i = 0; i < res.length; i++) {
                if (res[i].name === 'rent') {
                    this.$scope.rent = res[i];
                    break;
                }
            }
            for (let i = 0; i < res.length; i++) {
                if (res[i].name === 'wednesday') {
                    this.$scope.collectsOnWednesday = res[i];
                    break;
                }
            }

            $scope.products = _.filter(res,
                (product: any) =>
                    product.name !== 'deposit'
                    && product.name !== 'rent'
                    && product.name !== 'wednesday');

            $timeout(this.askForRent);
        });

        $scope.addProduct = () => {
            let newProduct = this.service.addProduct();
            $scope.products.push(newProduct);
            // TODO Focus correct column
            return newProduct;
        };

        hotkeys.bindTo($scope).add({
            combo: '+',
            description: 'Bestellung hinzufügen',
            callback: () => {
                $scope.addProduct();
            }
        });

        $scope.toggleCheatsheet = () => { hotkeys.toggleCheatSheet(); };
    }
}

angular.module('foodcoopApp').component('orderbook', {
    templateUrl: 'app/orderbook/orderbook.html',
    controller: OrderbookComponent
});

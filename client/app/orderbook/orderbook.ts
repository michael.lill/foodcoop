'use strict';

angular.module('foodcoopApp').config(function($routeProvider) {
  $routeProvider.when('/orderbook/:year/:cw',
                      {template : '<orderbook></orderbook>'});
});

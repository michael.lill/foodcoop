angular.module('foodcoopApp').config(function ($routeProvider) {
  $routeProvider.when(
    '/faq',
    { template: '<faq></faq>' });
});

class FaqComponent {
  private email: string = this.Auth.getCurrentUser().email;
  constructor(private Auth, private service: Service, private odsWriter: any) { }

  // tslint:disable-next-line:no-unused-variable
  private changeMail() {
    this.service.changeMail(this.email).then(() => {
      this.Auth.getCurrentUser().email = this.email;
    });
  }

  // tslint:disable-next-line:no-unused-variable
  private testMail() {
    this.service.testMail().then(() => toastr.info('Erfolgreich gesendet.'));
  }

  // tslint:disable-next-line:no-unused-variable
  private exportBook() {
    this.service.allMyProducts().then((resp) => {
      const allMyProducts = resp.data as any[];
      // Jahr gewichten: Sortieren nach Jahr dann nach KW
      const sortedProducts = allMyProducts
        .sort((a, b) => (a.year - b.year) * 100 + a.cw - b.cw);
      let balance = 0;
      let lastElement = { year: 0, cw: 0 };
      let result: any = [];
      result.push(['Jahr', 'Kalenderwoche', 'Name', 'Preis']);
      sortedProducts.forEach(element => {
        if (lastElement.year !== element.year || lastElement.cw !== element.cw) {
          result.push(['----', '----', '----', -balance]);
        }
        lastElement = element;
        balance += element.price;
        result.push([element.year, element.cw, Helper.elementNameFilter(element.name), -element.price]);
      });
      result.push(['----', '----', '----', -balance]);
      this.odsWriter.write({ 'Sheet1': result }, 'Haushaltsbuch.ods');
    });
  }
}

angular.module('foodcoopApp').component('faq', {
  template: `
    <uib-accordion>
    <div uib-accordion-group class="panel-default" heading="Kann ich mich benachrichtigen lassen, wenn die Bestelllisten eingetragen wurden?">
      <p>Ja. Dazu bitte hier die Mailadresse eintragen.</p>
      <div class="form-group">
          <label>Email</label>
          <input type="text" name="email" class="form-control"  ng-model-options="{ debounce: 500 }"  ng-change="$ctrl.changeMail()" ng-model="$ctrl.email">
      </div>
      <div class="form-group">
          <label></label>
          <button class="btn btn-default" ng-click="$ctrl.testMail()">Testmail versenden</button>
      </div>
    </div>
    <div uib-accordion-group class="panel-default" heading="Wie viel Miete muss ich pro Monat zahlen?">
      Pro erwachsene Person 5€ pro Monat. (bei einem 2-Personen-Haushalt -> 10€ usw.)
    </div>
    <div uib-accordion-group class="panel-default" heading="Kann ich mein Haushaltsbuch exportieren?">
      <button class="btn btn-default" ng-click="$ctrl.exportBook()">Haushaltsbuch exportieren</button>
    </div>
    <div uib-accordion-group class="panel-default" heading="Bis wann werden meine Bestellungen angenommen?">
      Bestellschluss ist immer Mittwoch 21:00.
    </div>
    <div uib-accordion-group class="panel-default" heading="Sicherheit / Backup / Technisches">
      Die Datenbank (SQLite) wird einmal pro Tag gesichert.
      Der Quelltext des Programms liegt hier: 
      <a href="https://gitlab.com/michael.lill/foodcoop">https://gitlab.com/michael.lill/foodcoop</a>
    </div>
        <div uib-accordion-group class="panel-default" heading="Wie kann ich Rückerstattungen eintragen?">
      In die Einkäufe können auch negative Zahlen eingegeben werden.
      Als Menge -1 wählen und als Preis den Auszahlungsbetrag.
    </div>
        <div uib-accordion-group class="panel-default" heading="Meine Bestellung wurde nicht bestellt. Woran kann es liegen?">
      <ul>
      <li>Es wurde für die falsche Woche bestellt.</li>
      <li>Es wurde zu spät bestellt.</li>
      <li>Die Software enthält Fehler. Hoffentlich nicht...</li>
      </ul>
    </div>
    <div uib-accordion-group class="panel-default" heading="Changelog. Änderungen an der Software">
      <ul>
        <li>"Mittwoch abholen" angebbar.</li>
        <li>Warenliste filterbar.</li>
        <li>Warenliste zeigt Infos über schon eingegangene Bestellmengen.</li>
      </ul>
    </div>
    <div uib-accordion-group class="panel-default" heading="Organigramm">
        <img src="/assets/images/Foodcoop.png" class="img-thumbnail" />
        <a href="/assets/Foodcoop.mm" target="_blank">Quelldatei (mit FreeMind erstellt.)</a>
    </div>
    <div uib-accordion-group class="panel-default" heading="Käsepreisliste">
      <a href="http://foodcoop-karlsruhe.de/kaesepreise.pdf" target="_blank">Käsepreisliste</a>    
    </div>
  </uib-accordion>
  `,
  controller: FaqComponent
});

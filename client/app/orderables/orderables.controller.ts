class OrderablesComponent {
  public modal =
  (title: string, body: string) => {
    return this.$uibModal.open({
      template: `
                    <div class="modal-header">
                        <h3 class="modal-title">${title}</h3>
                    </div>
                    <div class="modal-body">
                        ${body}
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
                        <button class="btn btn-warning" type="button" ng-click="cancel()">Abbrechen</button>
                    </div>
                `,
      controller: 'ModalController'
    });
  }

  private updateOrderDetails() {
    this.service.orderDetails().then(res => {
      this.$scope.orders = res.data;
    });
  }

  constructor(private $scope, private service: Service, private $uibModal, private focus, private Auth) {

    service.IsClosedForOrdersAsync().then(data => {
      $scope.IsClosedForOrders = data;
    });

    $scope.orderables = service.orderables().query(() => {
      $scope.orderables = _.sortBy(
        $scope.orderables, function (n: any) { return n.supplier; });
    });

    $scope.unit = function (orderable) {
      let result = orderable.name.split('♦');
      if (result.length > 2) {
        return result[1];
      }
      return '?';
    };

    this.updateOrderDetails();

    $scope.hasCurrentUserOrdered = (id: Number) => {
      if (!$scope.orders) {
        return undefined;
      }
      return ($scope.orders as Array<any>)
        .filter(value => value.OrderableId === id && value.UserId === this.Auth.getCurrentUser()._id)
        .reduce((prev, current) => prev + current.quantity, 0) > 0;
    };

    $scope.quantityOfOrders = (id: Number) => {
      if (!$scope.orders) {
        return undefined;
      }
      return ($scope.orders as Array<any>)
        .filter(value => value.OrderableId === id)
        .reduce((prev, current) => prev + current.quantity, 0);
    };

    $scope.fuzzy = (searchTerm: string, properties: Array<string>) => {
      return (obj: Object, index, array) => {
        if (typeof searchTerm !== typeof '') {
          return true;
        }
        let searchTerms = searchTerm.split(' ');
        let value = properties.reduce((previousValue: string, currentValue: string) => previousValue + obj[currentValue], '');
        return searchTerms.every(term => value.toLowerCase().indexOf(term.toLowerCase()) !== -1);
      };
    };

    $scope.addOrder = (orderable) => {
      let title = `Bitte die Menge angeben für <h3>${orderable.name}</h3>`;
      let body =
        `<input keybindings ng-init="answer=1" id="quantity" 
            ng-pattern-restrict="^-{0,1}\\d{0,4},{0,1}\\d{0,3}$"
            german-number type="text" 
            ng-model="answer" class="center-block form-control text-right" />`;
      let modal = this.modal(title, body);
      modal.rendered.then(() => {
        let inputQuantity = (document.getElementById('quantity') as HTMLInputElement);
        inputQuantity.select();
      });
      modal.result.then(answer => {
        let quantity = parseFloat(answer);
        if (!isNaN(quantity)) {
          service.addProduct(orderable.name + '♦' + orderable.supplier, quantity, orderable.price, orderable._id)
            .$promise
            .then(() => this.updateOrderDetails());
        }
      });

    };
  }
}

angular.module('foodcoopApp').component('orderables', {
  templateUrl: 'app/orderables/orderables.html',
  controller: OrderablesComponent
});

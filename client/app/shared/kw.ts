import moment from 'moment';

export class KW {
    constructor(public year: number, public cw: number) {
    }

    private calendarWeeksIn =
    (year) => { return moment(year + '-06-06').isoWeeksInYear(); }

    public next = () => {
        return this.cw === this.calendarWeeksIn(this.year) ?
            new KW(this.year + 1, 1) : new KW(this.year, this.cw + 1);
    }

    public prev = () => {
        return this.cw === 1 ? new KW(this.year - 1, this.calendarWeeksIn(this.year - 1))
            : new KW(this.year, this.cw - 1);
    }
}

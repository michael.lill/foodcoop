export class Helper {
    public static elementNameFilter(str: string) {
        if (str === 'deposit') {
            return 'Einzahlung';
        }
        if (str === 'rent') {
            return 'Miete';
        }
        return str ? str.replace('&', 'und') : str;
    }
}

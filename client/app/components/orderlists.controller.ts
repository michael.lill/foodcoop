
import * as toastr from 'toastr';

export class OrderlistsComponent {

  private applyCellMapping =
    (worksheet, mapping) => {
      this.setCell(worksheet, mapping[0], 'productName');
      this.setCell(worksheet, mapping[1], 'country');
      this.setCell(worksheet, mapping[2], 'association');
      this.setCell(worksheet, mapping[3], 'price');
      this.setCell(worksheet, mapping[4], 'unit');
    }

  private setCell(worksheet, cell, value) {
    if (typeof worksheet[cell] === 'undefined') {
      worksheet[cell] = { t: 's' };
    }
    let desiredCell = worksheet[cell];
    desiredCell.w = value;
    desiredCell.v = value;
  }

  private importWs =
    (worksheet) => {
      let worksheetAsJson = (window as any).XLSX.utils.sheet_to_json(worksheet);
      let filteredJson = _.filter(worksheetAsJson, this.isValid);
      filteredJson.reduce((promise, n) => {
        return promise.then(() => {
          let name = n['productName'] + '♦' + n['unit'] + '♦' +
            n['country'] + '♦' + n['association'];
          return this.importOrderable(name, n['price']);
        });
      }, Promise.resolve());
    }

  private SendNotifications() {
    if (this.$window.confirm('Sollen Nutzende, die es wünschen, per Mail über die neuen Einträge benachrichtigt werden?')) {
      this.service.sendNotifications()
        .then(
          (resp) => toastr.info(`Erfolgreich gesendet an ${(resp.data as any).usersNotified} Personen.`),
          () => toastr.error('Fehler beim Senden.')
        );
    }
  }

  private importOrderable = (name, price) => {
    let newOrderable = this.service.orderables().save({
      supplier: this.$scope.selectedSupplier,
      cw: this.service.getSelectedCw().cw,
      year: this.service.getSelectedCw().year,
      name: name,
      price: price
    });
    this.$scope.orderables.push(newOrderable);
    return newOrderable.$promise;
  };

  private isValid(row) {
    if (!(row['productName'] && row['country'] && row['association'] &&
      row['price'] && row['unit'])) {
      toastr.info("Ignoriert (fehlende notwendige Zelle nicht ausgefüllt): " + Object.values(row).join(' '));
      return false;
    }
    if (isNaN(row['price'])) {
      toastr.info("Ignoriert (Preis nicht als Zahl erkannt): " + Object.values(row).join(' : '));
      return false;
    }
    return true;
  }

  private isCrossedOut(cell) {
    return (cell.XF?.ifnt ?? 0) >= 30;
  }

  private setCrossedOutCellsToUndefined(worksheet) {
    for (let key in worksheet) {
      /* all keys that do not begin with "!" correspond to cell addresses */
      if (key[0] === '!') {
        continue;
      }
      if (this.isCrossedOut(worksheet[key])) {
        worksheet[key].w = undefined;
        worksheet[key].v = undefined;
      }
    }
  }

  private setSupplier =
    (worksheet) => {
      if (worksheet['B1'] && worksheet['B1'].w === 'Lieferdatum') {
        this.$scope.selectedSupplier = 'Petrik';
        return true;
      }
      if (worksheet['C1'] && worksheet['C1'].w === 'Lieferdatum') {
        this.setCrossedOutCellsToUndefined(worksheet);
        this.$scope.selectedSupplier = 'Blanc';
        return true;
      }
      if (worksheet['A1']) {
        this.$scope.selectedSupplier = worksheet['A1'].w;
        return this.$scope.suppliers.indexOf(this.$scope.selectedSupplier) > -1;
      } else {

      }
    }

  private selectAndApplyCellMapping =
    (worksheet) => {
      if (this.$scope.selectedSupplier === 'Petrik') {
        this.applyCellMapping(worksheet, ['B1', 'C1', 'D1', 'E1', 'F1']);
      } else if (this.$scope.selectedSupplier === 'Blanc') {
        this.applyCellMapping(worksheet, ['D1', 'G1', 'H1', 'I1', 'J1']);
      } else {
        this.applyCellMapping(worksheet, ['A1', 'B1', 'C1', 'D1', 'E1']);
      }
      this.importWs(worksheet);
    }

  private handleFile =
    (e) => {
      let files = e.target.files;
      for (let i = 0, f = files[i]; i !== files.length; ++i) {
        let reader = new FileReader();
        reader.onload = (fileInput: any) => {
          let data = fileInput.target.result;
          let workbook = (window as any).XLSX.read(data, { type: 'binary' });
          let firstSheetName = workbook.SheetNames[0];
          let worksheet = workbook.Sheets[firstSheetName];
          if (this.setSupplier(worksheet)) {
            this.selectAndApplyCellMapping(worksheet);
          } else {
            toastr.error('"' + worksheet['A1'].w + '" ist kein bekannter Lieferant.');
          }
        };
        reader.readAsBinaryString(f);
      }
    }

  constructor(private $scope, consts, private service: Service, private $window, private $cookies) {
    const xlsxScript = document.createElement('script')
    xlsxScript.src = 'assets/xlsx.full.min.js';
    document.head.appendChild(xlsxScript);

    $scope.orderables = service.orderables().query();
    $scope.suppliers = $window.localStorage.getItem('suppliers').split(',');
    $scope.selectedSupplier = $window.localStorage.getItem('suppliers').split(',')[0];
    $scope.addOrderable = function () {
      let newOrderable = service.orderables().save({
        supplier: $scope.selectedSupplier,
        cw: service.getSelectedCw().cw,
        year: service.getSelectedCw().year,
        name: '',
        price: 0
      });
      $scope.orderables.push(newOrderable);
    };

    $scope.notify = () => this.SendNotifications();

    document.getElementById('excel-file')
      .addEventListener('change', this.handleFile, false);

    $scope.delete = function (orderable, orderables, $index) {
      orderable.$remove()
        .then(() => orderables.splice($index, 1)
          , () => { });
    }
    $scope.deleteBySupplier = function (selectedSupplier) {
      var promises = $scope.orderables
        .filter(x => x.supplier == selectedSupplier)
        .map(element => element.$remove());
      Promise.all(promises).then(() => {
        $scope.orderables = service.orderables().query();
      }, () => {
        $scope.orderables = service.orderables().query();
      });
    }
  }
}

import * as toastr from 'toastr';
import { Service } from '../services/service.service';
import {Helper} from '../shared/helper';


export class FaqComponent {
  private email: string = this.Auth.getCurrentUser().email;
  constructor(private Auth, private service: Service, private odsWriter: any) { }

  // tslint:disable-next-line:no-unused-variable
  private changeMail() {
    this.service.changeMail(this.email).then(() => {
      this.Auth.getCurrentUser().email = this.email;
    });
  }

  // tslint:disable-next-line:no-unused-variable
  private testMail() {
    this.service.testMail().then(() => toastr.info('Erfolgreich gesendet.'));
  }

  // tslint:disable-next-line:no-unused-variable
  private exportBook() {
    this.service.allMyProducts().then((resp) => {
      const allMyProducts = resp.data as any[];
      // Jahr gewichten: Sortieren nach Jahr dann nach KW
      const sortedProducts = allMyProducts
        .sort((a, b) => (a.year - b.year) * 100 + a.cw - b.cw);
      let balance = 0;
      let lastElement = { year: 0, cw: 0 };
      let result: any = [];
      result.push(['Jahr', 'Kalenderwoche', 'Name', 'Preis']);
      sortedProducts.forEach(element => {
        if (lastElement.year !== element.year || lastElement.cw !== element.cw) {
          result.push(['----', '----', '----', -balance]);
        }
        lastElement = element;
        balance += element.price;
        result.push([element.year, element.cw, Helper.elementNameFilter(element.name), -element.price]);
      });
      result.push(['----', '----', '----', -balance]);
      this.odsWriter.write({ 'Sheet1': result }, 'Haushaltsbuch.ods');
    });
  }
}

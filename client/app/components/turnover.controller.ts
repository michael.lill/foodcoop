import { Service } from "../services/service.service";

export class TurnoverComponent {
    constructor($scope, private service: Service, moment) {
        if (!(window as any).Chart) {
            const script = document.createElement('script');
            script.onload = this.init;
            script.src = 'node_modules/chart.js/dist/Chart.min.js';
            document.head.appendChild(script);
        } else {
            this.init();
        }
    }

    private init = (): any => {
        this.service
            .getTurnover()
            .then(response => {
                const ctx = (document.getElementById('turnover-chart') as HTMLCanvasElement).getContext('2d');
                const myChart = new (window as any).Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: (response.data as any).map(x => `${x.year}-${x.cw}`),
                        datasets: [{
                            label: 'Umsatz (ohne Mietzahlungen)',
                            data: (response.data as any).map(x => x.turnover)
                        }]
                    }
                });

            });
    }
}

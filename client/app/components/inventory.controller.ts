
import { Helper } from '../shared/helper';
import { Service } from '../services/service.service';
declare var _: any;
declare var angular:any;

export class InventoryComponent {
  static isNullOrWhitespace(input) {
    if (typeof input === 'undefined' || input == null) return true;
    return input.replace(/\s/g, '').length < 1;
  }

  static HasMatch(category: any, name: string): any {
    const haveToMatch: Array<string> = category[1].split(',')
      .filter(s => !InventoryComponent.isNullOrWhitespace(s))
      .map((s: string) => s.trim());
    const mustNotMatch: Array<string> = category[2].split(',')
      .filter(s => !InventoryComponent.isNullOrWhitespace(s))
      .map((s: string) => s.trim());
    return haveToMatch.some(x => name.search(new RegExp(x, 'i')) >= 0)
      && (mustNotMatch.length === 0 || mustNotMatch.every(x => name.search(new RegExp(x, 'i')) < 0));
  }
  NameOfMatchingCategory(name: any): any {
    const matchingCategories = this.categories.value
      .split('\n')
      .map(x => x.split(';'))
      .filter(category => category.length === 3)
      .filter(category => InventoryComponent.HasMatch(category, name));
    return matchingCategories.length > 0 ? matchingCategories[0][0] : null;
  }
  HasMatchingCategory(name: any): any {
    return typeof this.NameOfMatchingCategory(name) === 'string';
  }
  private startbalances$: angular.IHttpPromise<{}>;
  private endbalances$: angular.IHttpPromise<{}>;
  private inventory$: angular.IHttpPromise<{}>;
  private categories: any = { value: 'Lade...' };
  private defaultCategories =
    `Durch Semikolons getrennt werden hier angegeben:
1) Kategoriename
2) Begriffe von denen mind. einer enhalten sein muss
3) optional: Begriffe, die nicht enthalten sein dürfen.
Käse;käse,fleisch,wurst,lyoner,speck,salami,rinderroh,rohesser,schabziger,saiger,schinken;
Bananen;banane,banana;milch
Honig;honig;waffel
Wein;wein,spätburgunder,regent,riesling,dornfelder,portugieser;essig
Kaffee;kaffee,espresso;
Olivenöl;olivenöl;
Tomaten;tomate;
Nudeln;Nudel,spaghetti,spirelli;
Müsli;haferflock,müsli;
Schokolade;schoki,schokolade,schoko;
Äpfele;äpfele, apfelsaft, Apfelsaft ,saft;Trauben
Secco;Secco, schnaps, prosecco, sekt, schneeweiß, rosenrot;`;

  private static YearDashCW = (c: any) => c.year + '-' + (c.cw < 10 ? `0${c.cw}` : c.cw);

  private Category = (x: any) =>
    x.name === 'deposit' ? 'Einzahlungen' :
      x.price < 0 ? 'negative Beträge' :
        x.name === 'rent' ? 'Miete' :
          x.Orderable ? x.Orderable.supplier :
            this.HasMatchingCategory(x.name) ? this.NameOfMatchingCategory(x.name) :
              x.price > 100 ? 'große Beträge >100€' :
                x.price > 20 ? 'mittlere Beträge >20€' :
                  'kleine Beträge';

  private byDateThenByCategory = (data) => {
    let result = _.groupBy(data, InventoryComponent.YearDashCW);
    for (let key in result) {
      if (result.hasOwnProperty(key)) {
        result[key] = _.groupBy(result[key], this.Category) as any;
      }
    }
    return result;
  }

  private byHHThenByDateThenByCategory = (data) => {
    let result = _.groupBy(data, (x: any) => x.User.name);
    for (let key in result) {
      if (result.hasOwnProperty(key)) {
        result[key] = _.groupBy(result[key], InventoryComponent.YearDashCW) as any;
      }
    }
    for (let key in result) {
      if (result.hasOwnProperty(key)) {
        for (let key2 in result[key]) {
          if (result[key].hasOwnProperty(key2)) {
            result[key][key2] = _.groupBy(result[key][key2], this.Category) as any;
          }
        }
      }
    }
    return result;
  }

  private byHHThenByCategory = (data) => {
    let result = _.groupBy(data, (x: any) => x.User.name);
    for (let key in result) {
      if (result.hasOwnProperty(key)) {
        result[key] = _.groupBy(result[key], this.Category) as any;
      }
    }
    return result;
  }

  findBalance(array: Array<any>, user: string): string {
    const ub = array.find(x => x.User.name === user);
    return (ub ? -ub.balance : 0).toFixed(2);
  }

  private mapBalances(start, end, byHH, calendarWeeks) {
    const users: Array<string> = _.uniq(start.map(b => b.User.name)
      .concat(Object.keys(byHH))
      .concat(end.map(b => b.User.name)));
    return [['Haushalt', 'Start-Balance'].concat(calendarWeeks).concat(['End-Balance'])]
      .concat(users
        .map(user => [user.replace('HH', ''), this.findBalance(start, user).toString()]
          .concat(calendarWeeks.map(cw => {
            let hh = byHH[user];
            if (!hh) {
              return '-';
            }
            return hh
              .filter(c => InventoryComponent.YearDashCW(c) <= cw)
              .reduceAndMap((a, b) => a - b.price, this.findBalance(start, user));
          }))
          .concat([this.findBalance(end, user).toString()])))
      .concat([['Alle Haushalte', start
        .reduceAndMap((a, b) => a - b.balance, 0)]
        .concat(calendarWeeks.map(y => '-'))
        .concat([end.reduceAndMap((a, b) => a - b.balance, 0)])]);
  }

  private async ExportODS() {
    (Array.prototype as any).reduceAndMap = function (reducer: Function, initValue: any) {
      let result = this.reduce(reducer, initValue);
      return {
        value: (typeof result === 'number') ? result.toFixed(2) : result
      };
    };

    const startbalances = (await this.startbalances$).data;
    const endbalances = (await this.endbalances$).data;
    const inventory = (await this.inventory$).data;
    const sortedInventory = (inventory as Array<any>)
      .sort(function (a, b) { return (a.year - b.year) * 100 + a.cw - b.cw; });
    const cws = _.uniq((sortedInventory as Array<any>).map(InventoryComponent.YearDashCW));
    const byHH = _.groupBy(sortedInventory, (x: any) => x.User.name);

    let data = {
      'Bestände': this.mapBalances(startbalances, endbalances, byHH, cws),
      'Alle Transaktionen': this.transactions(sortedInventory),
      'negative Beträge': this.negativeAmounts(sortedInventory),
      'Mieten': this.rents(sortedInventory, byHH),
      'Einzahlungen': this.paymentsIn(sortedInventory, byHH),
      'Nach Kategorie': this.byCategory(sortedInventory, cws),
      'Allgemeines': [['Von', 'Bis'], ['Nicht implementiert']],
    };

    for (let key in byHH) {
      if (byHH.hasOwnProperty(key)) {
        let element = byHH[key];
        data[key] = [['Jahr', 'KW', 'Name', 'Preis']];
        let products = element.filter((x: any) => x.name !== 'deposit' && x.name !== 'rent');
        products.forEach((x: any) => {
          data[key].push([x.year, x.cw, x.name, x.price]);
        });
        data[key].push(['---', '---', '---', '---']);
        data[key].push(['Gesamt', '---', '---', (products as any).reduceAndMap((a: any, b: any) => a + b.price, 0)]);
      }
    }

    this.odsWriter.write(data,
      `Iventurexport von ${this.$routeParams.startyear}_${this.$routeParams.startcw}` +
      ` bis ${this.$routeParams.endyear}_${this.$routeParams.endcw}.xlsx`);
  }

  private negativeAmounts(inventory) {
    let result = [['Jahr', 'KW', 'Haushalt', 'Name', 'Preis']];
    inventory
      .filter(x => x.price < 0)
      .forEach(x => result
        .push([x.year, x.cw, x.User.name.replace('HH', ''), Helper.elementNameFilter(x.name), x.price]));
    return result;
  }

  private transactions(inventory) {
    let result = [['Jahr', 'KW', 'Haushalt', 'Name', 'Preis']];
    inventory
      .forEach(x => result
        .push([x.year, x.cw, x.User.name.replace('HH', ''), Helper.elementNameFilter(x.name), x.price]));
    return result;
  }

  private rents(inventory, byHH) {
    let cwsContainingRent = _.uniq((inventory as Array<any>)
      .filter((x: any) => x.name === 'rent')
      .map(InventoryComponent.YearDashCW));
    let result = [['Haushalt'].concat(cwsContainingRent).concat(['Gesamt'])];
    for (let key in byHH) {
      if (byHH.hasOwnProperty(key)) {
        let element = byHH[key];
        result.push([key.replace('HH', '')].concat(
          cwsContainingRent.map(
            (cw: any) => ((element as Array<any>)
              .filter((c: any) => InventoryComponent.YearDashCW(c) === cw && c.name === 'rent') as any)
              .reduceAndMap((a: any, b: any) => a + b.price, 0)
          )).concat([((element as Array<any>)
            .filter((c: any) => c.name === 'rent') as any)
            .reduceAndMap((a: any, b: any) => a + b.price, 0)]));
      }
    }
    return result;
  }

  private paymentsIn(inventory, byHH) {
    let cwsContainingDeposit = _.uniq((inventory as Array<any>)
      .filter((x: any) => x.name === 'deposit')
      .map(InventoryComponent.YearDashCW));
    let result = [['Haushalt'].concat(cwsContainingDeposit).concat(['Gesamt'])];
    for (let key in byHH) {
      if (byHH.hasOwnProperty(key)) {
        let element = byHH[key];
        result.push([key.replace('HH', '')].concat(
          cwsContainingDeposit.map(
            (cw: any) => ((element as Array<any>)
              .filter((c: any) => InventoryComponent.YearDashCW(c) === cw && c.name === 'deposit') as any)
              .reduceAndMap((a: any, b: any) => a - b.price, 0)
          )).concat([((element as Array<any>)
            .filter((c: any) => c.name === 'deposit') as any)
            .reduceAndMap((a: any, b: any) => a - b.price, 0)]));
      }
    }
    return result;
  }

  private byCategory(inventory, cws) {
    let byCategory = _.groupBy(inventory, (x: any) => this.Category(x));
    let result = [['Kategorie'].concat(cws).concat(['Gesamt'])];
    for (let key in byCategory) {
      if (byCategory.hasOwnProperty(key)) {
        let element = byCategory[key];
        result.push([key]
          .concat(cws.map((cw: any) => (element
            .filter((c: any) => InventoryComponent.YearDashCW(c) === cw) as any)
            .reduceAndMap((a: any, b: any) => a + b.price, 0))
          )
          .concat([(element as any).reduceAndMap((a: any, b: any) => a + b.price, 0)]));
      }
    }
    return result;
  }

  private upsertCategories() {
    return this.service.upsertSetting(this.categories);
  }

  private resetCategories() {
    this.categories.value = this.defaultCategories;
    this.upsertCategories();
  }

  constructor(
    private $scope, private service: Service, private moment, private $routeParams,
    private $route, private odsWriter: any) {
    service.getSetting('customCategories').then((response) => {
      if ((response.data as Array<any>).length === 0) {
        return service.upsertSetting({
          key: 'customCategories',
          value: this.defaultCategories
        })
          .then((response) => {
            this.categories = response.data;
          });
      }
      this.categories = response.data[0];
    });
    $scope.c = {};
    angular.copy($routeParams, $scope.c);
    for (let key in $scope.c) {
      if ($scope.c.hasOwnProperty(key)) {
        $scope.c[key] = parseInt($scope.c[key]);
      }
    }

    $scope.cwToString = (year: string, cw: string) => {
      return moment(`${year}${cw}`, 'GGGGWW').format('DD.MM.YYYY');
    };

    $scope.sum = (s: any) =>
      _.reduce(s, (sum: number, n: any) => sum + n.price, 0);

    $scope.update = () => { $route.updateParams($scope.c); };

    this.startbalances$ = service.getBalances($routeParams.startyear, $routeParams.startcw - 1);
    this.endbalances$ = service.getBalances($routeParams.endyear, $routeParams.endcw);

    this.endbalances$.then((result) => {
      $scope.balances = result.data;
    });

    this.inventory$ = service
      .inventory($routeParams.startyear, $routeParams.startcw, $routeParams.endyear, $routeParams.endcw);

    this.inventory$.then(response => {
      let result = response.data;

      $scope.byDateThenByCategory = this.byDateThenByCategory(result);
      $scope.byHHThenByDateThenByCategory = this.byHHThenByDateThenByCategory(result);
      $scope.byHHThenByCategory = this.byHHThenByCategory(result);
      $scope.byCategory = _.groupBy(result, this.Category);
    });

  }
}
import filter from "lodash/filter";
import toastr from "toastr";
import { Service } from "../services/service.service";

export class NonperishablesComponent {

    private applyCellMapping =
        (worksheet, mapping) => {
            this.setCell(worksheet, mapping[0], 'productName');
            this.setCell(worksheet, mapping[1], 'country');
            this.setCell(worksheet, mapping[2], 'association');
            this.setCell(worksheet, mapping[3], 'price');
            this.setCell(worksheet, mapping[4], 'unit');
        }

    private setCell(worksheet, cell, value) {
        if (typeof worksheet[cell] === 'undefined') {
            worksheet[cell] = { t: 's' };
        }
        let desiredCell = worksheet[cell];
        desiredCell.w = value;
        desiredCell.v = value;
    }

    private importWs =
        (worksheet, supplier) => {
            let worksheetAsJson = (window as any).XLSX.utils.sheet_to_json(worksheet);
            let filteredJson = filter(worksheetAsJson, this.isValid);
            const matches = filteredJson.map(excelRow => {
                const matches = this.$scope.nonperishables
                    .filter(nonperishable =>
                        nonperishable.supplier === supplier
                        && nonperishable.name === excelRow['productName']
                        && nonperishable.unit === excelRow['unit']
                    );
                if (matches.length <= 0) {
                    return undefined;
                }
                return {
                    new: excelRow,
                    old: matches[0],      
                };
            }).filter(x => x !== undefined);
            if (window.confirm(
                `Bei Bestätigung werden diese Einträge überschrieben:\n` +
                matches.map(match => `${match.old.name}: ${match.old.pricePerQuantity}€ => ${match.new.price}€`).join('\n')
                )) {
                this.upsertNonPerishables(filteredJson, supplier);
            }
        }

    private upsertNonperishable = (supplier, excelRow) => {
        const matches = this.$scope.nonperishables
            .filter(nonperishable =>
                nonperishable.supplier === supplier
                && nonperishable.name === excelRow['productName']
                && nonperishable.unit === excelRow['unit']
            );
        if (matches.length > 0) {
            const match = matches[0];
            match.pricePerQuantity = excelRow['price'];
            return match.$update();
        }
        let newNonperishable = this.service.nonperishables().save({
            supplier: supplier,
            name: excelRow['productName'],
            pricePerQuantity: excelRow['price'],
            unit: excelRow['unit'],
        });
        this.$scope.nonperishables.push(newNonperishable);
        return newNonperishable.$promise;
    };

    private upsertNonPerishables(filteredJson: any[], supplier: any) {
        filteredJson.reduce((promise, excelRow) => {
            return promise.then(() => {
                return this.upsertNonperishable(supplier, excelRow);
            });
        }, Promise.resolve());
    }

    private isValid(row) {
        if (!(row['productName'] && row['price'] && row['unit'])) {
            toastr.info("Ignoriert (fehlende notwendige Zelle nicht ausgefüllt): " + Object.values(row).join(' '));
            return false;
        }
        if (isNaN(row['price'])) {
            toastr.info("Ignoriert (Preis nicht als Zahl erkannt): " + Object.values(row).join(' : '));
            return false;
        }
        return true;
    }

    private handleFile =
        (e) => {
            let files = e.target.files;
            for (let i = 0, f = files[i]; i !== files.length; ++i) {
                let reader = new FileReader();
                reader.onload = (fileInput: any) => {
                    let data = fileInput.target.result;
                    let workbook = (window as any).XLSX.read(data, { type: 'binary' });
                    let firstSheetName = workbook.SheetNames[0];
                    let worksheet = workbook.Sheets[firstSheetName];
                    const supplier = worksheet['A1'].w;
                    this.applyCellMapping(worksheet, ['A1', 'B1', 'C1', 'D1', 'E1']);
                    this.importWs(worksheet, supplier);
                };
                reader.readAsBinaryString(f);
            }
        }

    constructor(private $scope, private service: Service, moment) {
        const xlsxScript = document.createElement('script')
        xlsxScript.src = 'assets/xlsx.full.min.js';
        document.head.appendChild(xlsxScript);

        $scope.nonperishables = service.nonperishables().query();
        $scope.addNonperishable = function () {
            const newNonperishable = service.nonperishables().save({
                supplier: '',
                name: '',
                pricePerQuantity: 0
            });
            $scope.nonperishables.push(newNonperishable);
        };

        document.getElementById('excel-file')
            .addEventListener('change', this.handleFile, false);

        $scope.delete = function (nonperishable, nonperishables, $index) {
            nonperishable.$remove()
                .then(() => nonperishables.splice($index, 1)
                    , () => { });
        }
        $scope.germanDate = (date) => moment(date).format('DD.MM.yyyy');

        $scope.fuzzy = (searchTerm: string, properties: Array<string>) => {
            return (obj: Object, index, array) => {
                if (typeof searchTerm !== typeof '') {
                    return true;
                }
                let searchTerms = searchTerm.split(' ');
                let value = properties.reduce((previousValue: string, currentValue: string) => previousValue + obj[currentValue], '');
                return searchTerms.every(term => value.toLowerCase().indexOf(term.toLowerCase()) !== -1);
            };
        };
    }

}

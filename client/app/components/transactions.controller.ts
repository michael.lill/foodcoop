

export class TransactionsComponent {
  constructor($scope, service, moment) {
    $scope.data = service.transactions().query();
    $scope.month = function (transaction) {
      let ggggww = transaction.year.toString() + transaction.cw.toString();
      return moment(ggggww, ['GGGGWW']).add(1, 'days').format('MMMM');
    };
  }
}

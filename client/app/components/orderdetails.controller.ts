

export class OrderdetailsComponent {
  constructor($scope, service: Service) {
    $scope.data = true;
    $scope.monday = service.monday;
    service.orderDetails().then(function (res: any) {
      let filtered = _.filter(
        res.data, function (o: any) { return o.name !== 'wednesday'; });
      $scope.data = _.groupBy(filtered, function (n: any) {
        if (!n.User) {
          return '';
        }
        return n.User.name;
      });
    });
  }
}


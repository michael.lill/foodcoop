

export class OrdersummaryComponent {
  constructor($scope: any, service: Service) {
    $scope.monday = service.monday;
    service.orderDetails().then((res) => {
      $scope.orderers =
        _.map(_.groupBy(res.data, 'UserId'), (data: any) => {
          let user = data[0].User;
          if (user) {
            return user.name.substring(2);
          }
          return '';
        });

      $scope.wednesdayUsers = _.map(
        _.filter(res.data, ({ 'name': 'wednesday' } as any)),
        (product: any) => {
          if (product) {
            return product.User.name.substring(2);
          }
          return '';
        });

      $scope.groupedBySupplier = _.groupBy(
        _.filter(res.data, (x: any) => x.Orderable !== null),
        (x: any) => x.Orderable.supplier);

      for (let key in $scope.groupedBySupplier) {
        if ($scope.groupedBySupplier.hasOwnProperty(key)) {
          $scope.groupedBySupplier[key] =
            _.groupBy($scope.groupedBySupplier[key], 'OrderableId');
        }
      }

      $scope.suppliers =
        _.map(_.keys($scope.groupedBySupplier), (supplier: any) => {
          return {
            name: supplier,
            price: _.reduce(
              $scope.groupedBySupplier[supplier],
              (sum, users: any) => {
                return sum +
                  _.reduce(
                    users, (subsum, user: any) => subsum + user.price,
                    0);
              },
              0)
          };
        });

    });

    $scope.wednesday = (userName) => {
      return !!_.find(
        $scope.wednesdayUsers, function (o) { return o === userName; });
    };

    $scope.quantity = (supplier, products) => {
      return _.reduce(
        this.filterBySupplier(supplier, products),
        (sum, n: any) => { return sum + n.quantity; }, 0);
    };

    $scope.ordering = (supplier, products, userName) => {
      let filtered = this.filterByUser(
        this.filterBySupplier(supplier, products), userName);
      let result = _.reduce(
        filtered, (sum, n: any) => { return sum + n.quantity; }, 0);
      return result !== 0 ? result : '-';
    };

    service.balance().then((res) => { $scope.balance = res.data; });
  }

  private filterByUser = (products, userName) =>
    _.filter(products, (p: any) => {
      return (p.User ? p.User.name : p.User) === 'HH' + userName;
    });

  private filterBySupplier = (supplier, products) =>
    _.filter(products, (p: any) => {
      return (p.Orderable ? p.Orderable.supplier : '') === supplier;
    });
}

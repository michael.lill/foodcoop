import * as toastr from 'toastr';
import {Service} from '../services/service.service';

export class AdminComponent {
  constructor($scope, $location: angular.ILocationService, service: Service, auth) {
    $scope.getSelectedCw = service.getSelectedCw;
    $scope.$location = $location;

    $scope.isActive = function (route) {
      return route === $scope.$location.path();
    };

    $scope.setLock = () => service.getSetting('lockOrders').then((response) => {
      let template = ((response.data && response.data[0]) || {}) as any;
      template.key = 'lockOrders';
      template.value = JSON.stringify(service.getSelectedCw());
      service.upsertSetting(template)
        .then(() => toastr.success(`Bestellungen beendet für KW ${service.getSelectedCw().cw}`))
    });

    service.getUsers().then(response => {
      $scope.users = response.data;
    });

    $scope.setPassword = (userId, password) => {
      service.setPassword(userId, password).then(() => {
        toastr.success(`Passwort erfolgreich geändert.`);
      });
    };

    $scope.isAdmin = auth.isAdmin();

  }
}



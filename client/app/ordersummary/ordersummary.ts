'use strict';

angular.module('foodcoopApp').config(function($routeProvider) {
  $routeProvider.when('/ordersummary/:year/:cw',
                      {template : '<ordersummary></ordersummary>'});
});

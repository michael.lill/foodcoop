class OrderlistsComponent {

  private applyCellMapping =
  (worksheet, mapping) => {
    this.setCell(worksheet, mapping[0], 'productName');
    this.setCell(worksheet, mapping[1], 'country');
    this.setCell(worksheet, mapping[2], 'association');
    this.setCell(worksheet, mapping[3], 'price');
    this.setCell(worksheet, mapping[4], 'unit');
  }

  private setCell(worksheet, cell, value) {
    if (typeof worksheet[cell] === 'undefined') {
      worksheet[cell] = { t: 's' };
    }
    let desiredCell = worksheet[cell];
    desiredCell.w = value;
    desiredCell.v = value;
  }

  private importWs =
  (worksheet) => {
    console.log('Importing:');
    console.log(worksheet);
    let worksheetAsJson = (window as any).XLSX.utils.sheet_to_json(worksheet);
    let filteredJson = _.filter(worksheetAsJson, this.isValid);
    console.log('Filtered Json:');
    console.log(filteredJson);
    _(filteredJson)
      .forEach(async (n) => {
        let name = n['productName'] + '♦' + n['unit'] + '♦' +
          n['country'] + '♦' + n['association'];
        await this.importOrderable(name, n['price']);
      });
  }

  private SendNotifications() {
      if (!this.$scope.IsNotified) {
        if (this.$window.confirm('Sollen Nutzende, die es wünschen, per Mail über die neuen Einträge benachrichtigt werden?')) {
          this.service.sendNotifications()
            .then( 
              (resp) => toastr.info(`Erfolgreich gesendet an ${(resp.data as any).usersNotified} Personen.`),
              () => toastr.error('Fehler beim Senden.')
            );
        }
      }
  }

  private importOrderable = (name, price) => {
    let newOrderable = this.service.orderables().save({
      supplier: this.$scope.selectedSupplier,
      cw: this.service.getSelectedCw().cw,
      year: this.service.getSelectedCw().year,
      name: name,
      price: price
    });
    this.$scope.orderables.push(newOrderable);
    return newOrderable.$promise;
  };

  private isValid(row) {
    return row['productName'] && row['country'] && row['association'] &&
      row['price'] && row['unit'];
  }

  private isCrossedOut(cell) { return cell.XF.ifnt >= 30; }

  private setCrossedOutCellsToUndefined(worksheet) {
    for (let key in worksheet) {
      /* all keys that do not begin with "!" correspond to cell addresses */
      if (key[0] === '!') {
        continue;
      }
      if (this.isCrossedOut(worksheet[key])) {
        worksheet[key].w = undefined;
        worksheet[key].v = undefined;
      }
    }
  }

  private setSupplier =
  (worksheet) => {
    if (worksheet['B1'] && worksheet['B1'].w === 'Lieferdatum') {
      this.$scope.selectedSupplier = 'Petrik';
      return;
    }
    if (worksheet['C1'] && worksheet['C1'].w === 'Lieferdatum') {
      this.setCrossedOutCellsToUndefined(worksheet);
      this.$scope.selectedSupplier = 'Blanc';
      return;
    }
    this.$scope.selectedSupplier = 'Trockenwaren';
    return;
  }

  private selectAndApplyCellMapping =
  (worksheet) => {
    if (this.$scope.selectedSupplier === 'Petrik') {
      this.applyCellMapping(worksheet, ['B1', 'C1', 'D1', 'E1', 'F1']);
    } else if (this.$scope.selectedSupplier === 'Blanc') {
      this.applyCellMapping(worksheet, ['D1', 'G1', 'H1', 'I1', 'J1']);
    } else {
      toastr.error('Nur Petrik und Blanc können importiert werden.');
      return;
    }
    this.importWs(worksheet);
  }

  private handleFile =
  (e) => {
    console.log('Handeling File');
    let files = e.target.files;
    for (let i = 0, f = files[i]; i !== files.length; ++i) {
      let reader = new FileReader();
      reader.onload = (fileInput: any) => {
        console.log('Parsing File');
        let data = fileInput.target.result;
        let workbook = (window as any).XLSX.read(data, { type: 'binary' });
        let firstSheetName = workbook.SheetNames[0];
        let worksheet = workbook.Sheets[firstSheetName];
        this.setSupplier(worksheet);
        this.selectAndApplyCellMapping(worksheet);
      };
      reader.readAsBinaryString(f);
    }
  }

  constructor(private $scope, consts, private service: Service, private $window) {
    $scope.orderables = service.orderables().query();
    $scope.suppliers = consts.suppliers;
    $scope.selectedSupplier = consts.suppliers[0];
    $scope.addOrderable = function () {
      let newOrderable = service.orderables().save({
        supplier: $scope.selectedSupplier,
        cw: service.getSelectedCw().cw,
        year: service.getSelectedCw().year,
        name: '',
        price: 0
      });
      $scope.orderables.push(newOrderable);
    };

    $scope.IsNotified = true;    
    this.service.notificationStatus().then((res: any) => {
      $scope.IsNotified = res.data.status;
    });
    $scope.notify = () => this.SendNotifications();

    document.getElementById('excel-file')
      .addEventListener('change', this.handleFile, false);

    $scope.delete = function(orderable,orderables,$index){
      orderable.$remove()
        .then(() => orderables.splice($index,1)
          ,()=>{});
    }
  }
}

angular.module('foodcoopApp').component('orderlists', {
  templateUrl: 'app/orderlists/orderlists.html',
  controller: OrderlistsComponent
});

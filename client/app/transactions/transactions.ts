'use strict';

angular.module('foodcoopApp').config(function($routeProvider) {
  $routeProvider.when('/transactions/:year/:cw',
                      {template : '<transactions></transactions>'});
});

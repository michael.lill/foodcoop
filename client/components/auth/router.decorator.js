'use strict';
(function () {
    angular.module('foodcoopApp.auth')
        .run(function ($rootScope, $location, Auth, $routeParams, service) {
        // Redirect to login if route requires auth and the user is not logged in, or doesn't have required role
        $rootScope.$on('$routeChangeStart', function (event, next) {
            service.selectedCw.year = $routeParams.year;
            service.selectedCw.cw = $routeParams.cw;
            if (!next.authenticate) {
                return;
            }
            if (typeof next.authenticate === 'string') {
                Auth.hasRole(next.authenticate, _.noop).then(function (has) {
                    if (has) {
                        return;
                    }
                    event.preventDefault();
                    return Auth.isLoggedIn(_.noop).then(function (is) {
                        $location.path(is ? '/' : '/login');
                    });
                });
            }
            else {
                Auth.isLoggedIn(_.noop).then(function (is) {
                    if (is) {
                        return;
                    }
                    event.preventDefault();
                });
            }
        });
    });
})();
//# sourceMappingURL=router.decorator.js.map
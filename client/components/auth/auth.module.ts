'use strict';

angular
    .module(
        'foodcoopApp.auth',
        [ 'foodcoopApp.constants', 'foodcoopApp.util', 'ngCookies', 'ngRoute' ])
    .config(function($httpProvider) {
      $httpProvider.interceptors.push('authInterceptor');
    });

'use strict';

class NavbarController {
  // start-non-standard
  public menu = [{ 'title': 'Home', 'link': '/login' }];

  public isCollapsed = true;
  // end-non-standard

  private getSelectedCw: KW;
  private isLoggedIn: boolean;
  private isAdmin: boolean;
  private getCurrentUser: any;

  constructor(private $location, Auth, service) {
    this.getSelectedCw = service.getSelectedCw;
    this.isLoggedIn = Auth.isLoggedIn;
    this.isAdmin = Auth.isAdmin;
    this.getCurrentUser = Auth.getCurrentUser;
  }

  public isActive(route) { return route === this.$location.path(); }
}

angular.module('foodcoopApp').controller('NavbarController', NavbarController);

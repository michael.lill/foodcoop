#!/bin/sh
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
docker run -it --name foodcoop --rm -v"$SCRIPTPATH":/source -p9000:9000 -w /source node:11-alpine /bin/sh

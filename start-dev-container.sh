#!/bin/bash
set -euo pipefail

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

while [[ "$#" -gt 0 ]]; do
    case $1 in
        -p|--port) port="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

if [ -z ${port+x} ];
    then echo "-p|--port is unset";
    exit 1;
fi

docker-compose down
docker-compose run -p$port:9000 -p9229:9229 -w /source foodcoop /bin/bash
docker-compose down
